<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"
	scope="request" />
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="${ctx}/css/main.css">
<title>demo bill download</title>
</head>
<style>
.title{margin-left:50px;margin-top:30px; color:red;line-height: 30px;font-size: 24px;}
.description{margin-left:50px;color:red;line-height: 30px;margin-bottom: 50px;}
</style>
<body>
	<form method="post" action="${ctx}/doDownloadBill.htm" id="queryTradeForm" accept-charset="utf-8">
		<div class="content">
			<p class="title">商户对账单下载</p>
			<p class="description">每天10点之后才能下载前一日的对账单</p>
			<div class="content_0">
				<label>交易类型-tradeType:</label>
				<input type="text"  name="tradeType" value="cs.bill.download" maxlength="32" /> <br/>
				
				<label>接口版本-version:</label>
				<input type="text"  name="version" value="1.3" maxlength="8" /><br/>
				
				<label>商户号-mchId:</label>
				<input type="text"  name="mchId" value="${mchId}" maxlength="32" /><br/>
				
				
				<label>对账日期-billDate:</label>
				<input type="text"  name="billDate" value="${billDate }" placeholder="请输入商户订单号" maxlength="32" /><br/>

				<input type="submit" value="下载" class="btn1"><br/>
			</div>
		</div>
	</form>
</body>
</html>
