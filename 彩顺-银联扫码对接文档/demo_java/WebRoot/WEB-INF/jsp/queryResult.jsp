<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"
	scope="request" />

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title></title>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
	<div class="content" align="center">
	
   		<label> 返回状态码: </label>
		<label> ${returnMap.returnCode}</label><br />
		
   		<label> 返回信息: </label>
		<label> ${returnMap.returnMsg}</label><br />
		
		<c:if test="${returnMap.resultCode == '0'}">
			<label> 业务结果: </label>
			<label> ${returnMap.resultCode}</label><br />
			
	   		<label> 支付渠道: </label>
			<label> ${returnMap.channel}</label><br />
			
			<label> 商户订单号: </label>
			<label> ${returnMap.oTradeNum}</label><br />
			
			<label> 商品描述: </label>
			<label> ${returnMap.body}</label><br />
	   
	   		<label> 交易金额: </label>
			<label> ${returnMap.amount}</label><br />
			
			<label> 交易时间: </label>
			<label> ${returnMap.transTime}</label><br />
			
			<label> 交易状态: </label>
			<label> ${returnMap.transTime}</label><br />
			
			<label> 支付通道类型: </label>
			<label> ${returnMap.status}</label><br />
			
			<label> 支付渠道退款单号:</label>
			<label>${returnMap.payChannelType}</label><br />
			
			<label> 商品的标题:</label>
			<label>${returnMap.subject}</label><br />
		
			<label> 附加数据:</label>
			<label>${returnMap.description}</label><br />
		
			<label> 货币类型:</label>
			<label>${returnMap.currency}</label><br />
		
			<label> 附言:</label>
			<label>${returnMap.postscript}</label><br />
		</c:if>
	
		<c:if test="${returnMap.resultCode == '1'}">
			<label> 错误代码:</label>
			<label>${returnMap.errCode} </label><br />
			
			<label> 错误代码描述:</label>
			<label>${returnMap.errCodeDes} </label><br />
		</c:if>

	</div>
</body>
</html>
