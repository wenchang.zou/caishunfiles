<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"
	scope="request" />

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>demo refund apply result</title>
<link rel="stylesheet" type="text/css" href="${ctx }/css/main.css">
</head>
</head>
<body>
	<div class="content" align="center">
   		<label> 返回状态码: </label>
		<label> ${returnMap.returnCode}</label><br />
		
   		<label> 返回信息: </label>
		<label> ${returnMap.returnMsg}</label><br />
		
		<c:if test="${returnMap.resultCode == '0'}">
			<label> 业务结果: </label>
			<label> 成功</label><br />
			
	   		<label> 商户退款单号: </label>
			<label> ${returnMap.tradeNum}</label><br />
			
			<label> 商户订单号: </label>
			<label> ${returnMap.oTradeNum}</label><br />
			
			<label> 支付渠道退款单号:</label>
			<label>${returnMap.currency}</label><br />
		</c:if>
		
		<c:if test="${returnMap.resultCode == '1'}">
			<label> 业务结果: </label>
			<label> 失败</label><br />
		
	   		<label> 错误代码:</label>
			<label>${returnMap.errCode} </label><br />
			
			<label> 错误代码描述:</label>
			<label>${returnMap.errCodeDes} </label><br />
		</c:if>
		
	</div>
</body>
</html>
