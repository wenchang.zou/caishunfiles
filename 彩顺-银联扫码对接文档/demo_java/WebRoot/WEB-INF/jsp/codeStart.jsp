<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"
	scope="request" />
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="${ctx}/css/main.css">
</head>
<style>
.title{margin-left:50px;margin-top:30px; color:red;line-height: 30px;font-size: 24px;}
.description{margin-left:50px;color:red;line-height: 30px;margin-bottom: 50px;}
</style>
<body>
	<form method="post" action="${ctx}/genQRcode.htm" >
		<div class="content">
			<p class="title">生成收款二维码</p>
			<p class="description">又称固定二维码，用户可以通过微信、支付宝等扫描该二维码向商家支付（纯线下）</p>
			<div class="content_0">
				<label>交易类型-tradeType:</label>
				<input type="text"  name="tradeType" value="cs.pay.qrcode" maxlength="32" /> <br/>
				
				<label>接口版本-version:</label>
				<input type="text"  name="version" value="1.0" maxlength="8" /><br/>
				<label>代理商号-mchId:</label>
				<input type="text"  name="mchId" value="${mchId}" maxlength="32" /><br/>
				<label>商户号-subMchId:</label>
				<input type="text"  name="subMchId" value="${subMchId}" maxlength="32" /><br/>
				<input type="submit" value="生成" class="btn1"><br/>
				
			</div>
		</div>
				
	</form>
</body>

</html>
