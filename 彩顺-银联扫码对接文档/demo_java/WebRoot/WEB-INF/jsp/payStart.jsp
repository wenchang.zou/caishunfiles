﻿<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"
	scope="request" />
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="expires" content="0" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/main.css">
<script type="text/javascript" src="${ctx}/js/jquery.js"></script>
<title>demo create order</title>
</head>
<style>
.title{margin-left:50px;margin-top:30px; color:red;line-height: 30px;font-size: 24px;}
.description{margin-left:50px;color:red;line-height: 30px;margin-bottom: 50px;}
</style>
<body>
	<form action="${ctx}/createPay.htm" method="post" target="_blank">
		<div class="content">
			<p class="title">收银台下单接口</p>
			<p class="description">一个接口可以支付多种支付方式</p>
			<div class="content_0">
				<label>交易类型-tradeType:</label>
				<input type="text" name="tradeType" value="cs.pay.submit"> <br/>
				
				<label>版本-version:</label>
				<input type="text" name="version" value="1.0"> <br/>
				
				<label>支付渠道-channel:</label>
				<select id="channel" onchange="changeChannel(this)" name="channel">	
					<option value="wxPub">微信公众账号支付</option>
					<option value="wxPubQR">微信扫码支付</option>
					<option value="wxMicro">微信付款码支付</option>
					<option value="alipayQR">支付宝扫码支付</option>
					<option value="alipayMicro">支付宝付款码支付</option>
					<option value="jdMicro">京东付款码支付</option>
					<option value="jdQR">京东扫码支付</option>
				</select><br/>
				
				<label>商户号-mchId:</label>
				<input type="text"  name="mchId" value="${mchId}" maxlength="32" /><br/>
				
				
				<label>商品描述-body:</label>
				<input type="text" name="body" value="测试"> <br/>
				
				<label>商户订单号-outTradeNo:</label>
				<input type="text" name="outTradeNo" value="${outTradeNo }"> <br/>
				
				<label>交易金额-amount:</label>
				<input type="text" name="amount" value="0.01"> <br/>
				
				<label>附加数据-description:</label>
				<input type="text" name="description" value=""> <br/>
				
				<label>货币类型-currency:</label>
				<input type="text" name="currency" value="CNY"> <br/>
				
				<label>订单支付时间-timePaid:</label>
				<input type="text" name="timePaid" value=""> <br/>
				
				<label>订单失效时间-timeExpire:</label>
				<input type="text" name="timeExpire" value=""> <br/>
				
				<label>商品的标题-subject:</label>
				<input type="text" name="subject" value=""> <br/>
				<div id="extral">
				
				</div>
				<input type="submit" value="下单" id="showlayerButton" class="btn1">
				
			</div>
		</div>
	</form>
    <div>
    			<div id="wxPub"  style="display:none">
					<label>公众号类型-isRaw:</label>
					<select id="channel" onchange="changeChannel(this)" name="channel">	
						<option value="1">原生公众号</option>
						<option value="0">待封装</option>
					</select><br/>
					<label>子商户公众号标识-subAppId:</label>
					<input type="text" name="subAppId" value=""> <br/>
					<label>子商户公众号用户openid-subOpenId:</label>
					<input type="text" name="subOpenId" value=""> <br/>
					<label>微信支付分配的子商户号-wxSubMchId:</label>
					<input type="text" name="wxSubMchId" value=""> <br/>
					<label>指定支付方式-limitPay:</label>
					<input type="text" name="limitPay" value=""> <br/>
					<label>成功跳转url-callbackUrl:</label>
					<input type="text" name="callbackUrl" value=""> <br/>
					<label>结果通知url-notifyUrl:</label>
					<input type="text" name="notifyUrl" value=""> <br/>
					
				</div>
					
				<div id="wxPubQR"  style="display:none">
					<label>指定支付方式-limitPay:</label>
					<input type="text" name="limitPay" value=""> <br/>
					<label>结果通知url-notifyUrl:</label>
					<input type="text" name="notifyUrl" value=""> <br/>
				</div>
				<div id="wxMicro" style="display:none">
					<label>授权码-authCode:</label>
					<input type="text" name="authCode" value=""> <br/>
					<label>结果通知url-notifyUrl:</label>
					<input type="text" name="notifyUrl" value=""> <br/>
				</div>
				
				<div id="jdPay" style="display:none">
					<label>支付成功跳转路径url-callbackUrl:</label>
					<input type="text" name="callbackUrl" value=""> <br/>
					
					<label>支付完成后结果通知url-notifyUrl:</label>
					<input type="text" name="notifyUrl" value=""> <br/>
				</div>
				
				<div id="jdQR" style="display:none">
					<label>支付完成后结果通知url-notifyUrl:</label>
					<input type="text" name="notifyUrl" value=""> <br/>
					<label>支付完成后结果通知url-notifyUrl:</label>
				</div>
				<div id="jdMicro" style="display:none">
					<label>支付完成后结果通知url-notifyUrl:</label>
					<input type="text" name="notifyUrl" value=""> <br/>
				</div>
				<div id="alipayMicro" style="display:none">
					<label>支付完成后结果通知url-notifyUrl:</label>
					<input type="text" name="notifyUrl" value=""> <br/>
				</div>
    </div>
</body>
 <script type="text/javascript">
 	jQuery(function(){
 		changeChannel($('#channel').get(0));
 	});
 	function changeChannel(chan){
 		debugger;
 		$('#extral').html($('#' + chan.value).html());
	}
 </script>

</html>

