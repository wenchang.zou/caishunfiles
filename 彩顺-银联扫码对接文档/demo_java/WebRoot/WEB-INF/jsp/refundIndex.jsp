<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"
	scope="request" />
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="expires" content="0" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/main.css">
<title>demo refund apply</title>

</head>
<style>
.title{margin-left:50px;margin-top:30px; color:red;line-height: 30px;font-size: 24px;}
.description{margin-left:50px;color:red;line-height: 30px;margin-bottom: 50px;}
</style>
<body>
	<form method="post" action="${ctx }/refundApply.htm" id="refundForm">
		<div class="content">
			<p class="title">退款申请接口</p>
			<p class="description">根据商户订单号退款，注意，京东不支持退款</p>
			<div class="content_0">
					<label>交易类型-tradeType:</label>
					<input type="text" name="tradeType" value="cs.refund.apply" maxlength="32" /> <br/>
			
					<label>接口版本-version:</label>
					<input type="text" name="version" value="1.0" maxlength="8" /><br/>

					<label>商户号-mchId:</label>
					<input type="text"  name="mchId" value="${mchId}" maxlength="32" /><br/>
					

					<label>渠道类型-channel:</label>
					<select id="channel" onchange="changeChannel(this)" name="channel">	
					    <option value="0">---请选择---</option>
						<option value="weixin">微信</option>
						<option value="jd">京东</option>
						<option value="alipay">支付宝</option>
						<option value="baidu">百度</option>
					</select><br/>

					<label>商户订单号-outTradeNo:</label>
					<input type="text" name="outTradeNo" value="" placeholder="请输入商户订单号" maxlength="32" /><br/>

					<label>商户退款单号-outRefundNo:</label>
					<input type="text" name="outRefundNo" value=""  placeholder="请输入商户退款单号" maxlength="32" /><br/>

					<label>退款金额-amount:</label>
					<input type="text" name="amount" value=""  placeholder="请输入退款金额" /><br/>

					<label>退款详情-description:</label>
					<input type="text" name="description" value=""  placeholder="请输入退款详情" /><br/>
					
					<div id="wxPub" style="display:none">
						<label>指定支付方式-limitPay:</label>
						<input type="text" name="limitPay" value=""> <br/>
						
						<label>openId:</label>
						<input type="text" name="openId" value=""> <br/>
						
						<label>结果通知url-notifyUrl:</label>
						<input type="text" name="notifyUrl" value=""> <br/>
						
						<label>商品标记-goodsTag:</label>
						<input type="text" name="goodsTag" value=""> <br/>
					</div>
						
					<div id="wxPubQR" style="display:none">
						<label>指定支付方式-limitPay:</label>
						<input type="text" name="limitPay" value=""> <br/>
						
						<label>商品id-productId:</label>
						<input type="text" name="productId" value=""> <br/>
						
						<label>结果通知url-notifyUrl:</label>
						<input type="text" name="notifyUrl" value=""> <br/>
						
						<label>商品标记-goodsTag:</label>
						<input type="text" name="goodsTag" value=""> <br/>
					</div>
					
					<div id="wxApp" style="display:none">
						<label>指定支付方式-limitPay:</label>
						<input type="text" name="limitPay" value=""> <br/>
						
						<label>结果通知url-notifyUrl:</label>
						<input type="text" name="notifyUrl" value=""> <br/>
						
						<label>商品标记-goodsTag:</label>
						<input type="text" name="goodsTag" value=""> <br/>
					</div>
					
					<div id="wxMicro" style="display:none">
						<label>授权码-authCode:</label>
						<input type="text" name="limitPay" value=""> <br/>
						
						<label>结果通知url-notifyUrl:</label>
						<input type="text" name="notifyUrl" value=""> <br/>
						
						<label>商品标记-goodsTag:</label>
						<input type="text" name="goodsTag" value=""> <br/>
					</div>
					
					<div id="jdPay" style="display:none">
						<label>支付成功跳转路径url-callbackUrl:</label>
						<input type="text" name="callbackUrl" value=""> <br/>
						
						<label>支付完成后结果通知url-notifyUrl:</label>
						<input type="text" name="notifyUrl" value=""> <br/>
					</div>
					
					<div id="jdPayGate" style="display:none">
						<label>支付成功跳转路径url-callbackUrl:</label>
						<input type="text" name="callbackUrl" value=""> <br/>
						
						<label>支付完成后结果通知url-notifyUrl:</label>
						<input type="text" name="notifyUrl" value=""> <br/>
					</div>
					
					<div id="jdMicro" style="display:none">
						<label>支付完成后结果通知url-notifyUrl:</label>
						<input type="text" name="notifyUrl" value=""> <br/>
					</div>
					
					<div id="jdQR" style="display:none">
					</div>
					
					<input type="submit" value="退款申请" class="btn1">
				
			</div>
		</div>
	</form>
</body>
 <script type="text/javascript">
 	function changeChannel(chan){
	    for(var i=1;i<chan.length;i++)
	    {
	    	var div = document.getElementById(chan.options[i].value);
	    	div.style.display="none";
	    } 
 		if(chan.value != 0){
 			document.getElementById(chan.value).style.display="";
 		}
	}
 </script>

</html>
