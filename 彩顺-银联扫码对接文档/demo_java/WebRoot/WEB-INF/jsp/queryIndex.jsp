<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"
	scope="request" />
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="${ctx}/css/main.css">
<title>demo trade query</title>
</head>
<style>
.title{margin-left:50px;margin-top:30px; color:red;line-height: 30px;font-size: 24px;}
.description{margin-left:50px;color:red;line-height: 30px;margin-bottom: 50px;}
</style>
<body>
	<form method="post" action="${ctx}/queryOrder.htm" id="queryTradeForm">
		<div class="content">
			<p class="title">交易详情查询接口</p>
			<p class="description">根据商户订单号查询单笔订单，注意，一定要传查询类型queryType</p>
			<div class="content_0">
				<label>交易类型-tradeType:</label>
				<input type="text"  name="tradeType" value="cs.trade.single.query" maxlength="32" /> <br/>
				
				<label>接口版本-version:</label>
				<input type="text"  name="version" value="1.0" maxlength="8" /><br/>
				
				<label>商户号-mchId:</label>
				<input type="text"  name="mchId" value="${mchId}" maxlength="32" /><br/>
				
				<label>商户订单号-outTradeNo:</label>
				<input type="text"  name="outTradeNo" value="" placeholder="请输入商户订单号" maxlength="32" /><br/>
				
				<label>商户原交易订单号-oriTradeNo:</label>
				<input type="text"  name="oriTradeNo" value="" placeholder="请输入商户原交易订单号" maxlength="32" /><br/>

				<label>查询类型-queryType:</label>
				<select id="queryType" name="queryType">	
				    <option value="0">-请选择-</option>
					<option value="1">订单查询</option>
					<option value="3">退款查询</option>
				</select><br/>

				<input type="submit" value="查询" class="btn1"><br/>
			</div>
		</div>
	</form>
</body>
</html>
