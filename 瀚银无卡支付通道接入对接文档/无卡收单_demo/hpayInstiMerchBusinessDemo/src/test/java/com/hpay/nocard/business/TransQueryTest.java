package com.hpay.nocard.business;

import com.hpay.core.common.HttpPostUtil;
import com.hpay.core.common.HttpServiceException;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpException;
import org.junit.Test;

import com.hpay.core.common.util.MD5;

public class TransQueryTest {
	
	@Test
	public void apply(){
		try {
			Map<String, String> formparams=new HashMap<String, String>();
			formparams.put("insCode", "80000303");
			formparams.put("insMerchantCode", "887581298600948");
			formparams.put("hpMerCode", "HBMSDTDIRWK8I@20180611112111");
			formparams.put("orderNo", "17041400075800002657");			
			formparams.put("transDate", "20170614121212");			
			formparams.put("transSeq", "500032000");
			formparams.put("paymentType", "2008");
			formparams.put("productType", "100000");
			formparams.put("nonceStr", "123");
			formparams.put("signature", getSignnatrue(formparams));
			
			
			HttpPostUtil util=new HttpPostUtil();
			util.setIsignoreHttps(true);
			util.initHttp();
			String resp=util.post(formparams, "https://gateway.handpay.cn/hpayTransGatewayWeb/trans/query.htm");
			System.out.println(resp);
		} catch (HttpServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static String getSignnatrue(Map<String, String> formparams){
		StringBuilder sb=new StringBuilder();
		String signKey="6C6488F20D7072A9C3AAF20798481962";
		sb.append(formparams.get("insCode")).append("|")
				.append(formparams.get("insMerchantCode")).append("|")
				.append(formparams.get("hpMerCode")).append("|")
				.append(formparams.get("orderNo")).append("|")
				.append(formparams.get("transDate")).append("|")
				.append(formparams.get("transSeq")).append("|")
								.append(formparams.get("productType")).append("|")
				.append(formparams.get("paymentType")).append("|")

 	      		.append(formparams.get("nonceStr")).append("|")
				.append(signKey);
		

		
		MD5 md5=new MD5();
		return md5.getMD5ofStr(sb.toString());

	}
	
}
