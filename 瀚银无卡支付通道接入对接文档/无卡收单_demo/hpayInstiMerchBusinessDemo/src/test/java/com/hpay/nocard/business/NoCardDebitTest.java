package com.hpay.nocard.business;


import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpException;
import org.junit.Test;

import com.hpay.core.common.HttpPostUtil;
import com.hpay.core.common.HttpServiceException;
import com.hpay.core.common.util.DateUtils;
import com.hpay.core.common.util.MD5;

public class NoCardDebitTest {
	
	@Test
	public void apply(){
		try {
			/*{insCode=80000303, 
					orderNo=HY2018061500000198797532, 
					backUrl=http://20974003wc.iok.la:20387/caishun/notify/notifyHanyin.do, 
						signature=67F30FC3F9889671E2D1F6F4980D0308,
						accNo=6214837210574152, 
						insMerchantCode=887581298600948,
                      frontUrl=http://www.baidu.com,
                    	  idNumber=43252419870105675x, 
                    	  telNo=15111191925, 
                    	  nonceStr=fkdj, 
                    	  hpMerCode=HBMSDTDIRWK8I@20180611112111, 
                    	  paymentType=2008, 
                    	  orderTime=20180615100752, 
                    	  orderAmount=102, 
                    	  name=彭再锋, 
                    	  currencyCode=156, 
                    	  productType=100000}
			}*/
			Map<String, String> formparams=new HashMap<String, String>();
			formparams.put("insCode", "80000303");
			formparams.put("insMerchantCode", "887581298600948");
			formparams.put("hpMerCode", "HBMSDTDIRWK8I@20180611112111");
			formparams.put("orderNo", "2018061500001535188395");			
			formparams.put("orderTime", DateUtils.getStringForDate());
			formparams.put("currencyCode", "156");
			formparams.put("orderAmount", "10200");
			formparams.put("name", "彭再锋");
			formparams.put("idNumber", "43252419870105675x");
			formparams.put("accNo", "6214837210574152");
			formparams.put("telNo", "15111191925");
			formparams.put("productType", "100000");
			formparams.put("paymentType", "2008");
//			formparams.put("nonceStr", "1");
			formparams.put("frontUrl", "http://www.baidu.com");
			formparams.put("backUrl", "http://20974003wc.iok.la:20387/caishun/notify/notifyHanyin.do");
			formparams.put("nonceStr", "fkdj");
			formparams.put("signature", getSignnatrue(formparams));
			
			HttpPostUtil util=new HttpPostUtil();
			util.setIsignoreHttps(true);
			util.initHttp();
			String resp=util.post(formparams, "https://gateway.handpay.cn/hpayTransGatewayWeb/trans/debit.htm");
			System.out.println("12334gfo="+resp);
		} catch (HttpServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	//insCode|insMerchantCode|hpMerCode|orderNo|orderTime|orderAmount|name|idNumber|accNo| telNo|productType|paymentType| nonceStr|signKey
	public static String getSignnatrue(Map<String, String> formparams){
		StringBuilder sb=new StringBuilder();
		String signKey="6C6488F20D7072A9C3AAF20798481962";
		sb.append(formparams.get("insCode")).append("|")
				.append(formparams.get("insMerchantCode")).append("|")
				.append(formparams.get("hpMerCode")).append("|")
				.append(formparams.get("orderNo")).append("|")
				.append(formparams.get("orderTime")).append("|")
				.append(formparams.get("orderAmount")).append("|")
				.append(formparams.get("name")).append("|")
				.append(formparams.get("idNumber")).append("|")
				.append(formparams.get("accNo")).append("|")
				.append(formparams.get("telNo")).append("|")
				.append(formparams.get("productType")).append("|")
				.append(formparams.get("paymentType")).append("|")
				.append(formparams.get("nonceStr")).append("|")
				.append(signKey);
		
		MD5 md5=new MD5();
		System.out.println(md5.getMD5ofStr(sb.toString()));
		return md5.getMD5ofStr(sb.toString());

	}
	
}
