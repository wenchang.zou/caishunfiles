package com.hpay.nocard.business;


import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpException;
import org.junit.Test;

import com.hpay.core.common.HttpPostUtil;
import com.hpay.core.common.HttpServiceException;
import com.hpay.core.common.util.MD5;

public class CardWithdrawTest {
	
	@Test
	public void apply(){
		try {
			Map<String, String> formparams=new HashMap<String, String>();
			formparams.put("insCode", "80000001");
			formparams.put("insMerchantCode", "887581298600001");
			formparams.put("hpMerCode", "XKPOSP@201704140003185");
			formparams.put("orderNo", "17041400075800002657");			
			formparams.put("transDate", "20170414");			
			formparams.put("transAmount", "1");
			formparams.put("signature", getSignnatrue(formparams));
			
			
			
			
			
			HttpPostUtil util=new HttpPostUtil();
			util.setIsignoreHttps(true);
			util.initHttp();
			String resp=util.post(formparams, "http://10.148.181.155:8080/hpayTransGatewayWeb/withdraw/apply.htm");
			System.out.println(resp);
		} catch (HttpServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	public static String getSignnatrue(Map<String, String> formparams){
		StringBuilder sb=new StringBuilder();
		String signKey="2FC19851227CD17066E9AA5894740504";
		sb.append(formparams.get("insCode")).append("|")
				.append(formparams.get("insMerchantCode")).append("|")
				.append(formparams.get("hpMerCode")).append("|")
				.append(formparams.get("orderNo")).append("|")
				.append(formparams.get("transDate")).append("|")
				.append(formparams.get("transAmount")).append("|")
				.append(signKey);
		
		MD5 md5=new MD5();
		System.out.println(md5.getMD5ofStr(sb.toString()));
		return md5.getMD5ofStr(sb.toString());

	}
	
}
