package com.hpay.nocard.business;



import java.util.HashMap;
import java.util.Map;

import org.junit.Test;


import com.hpay.core.common.HttpPostUtil;
import com.hpay.core.common.util.MD5;


/**
 * 有卡交易商户进件
 * @author jili
 *
 */
public class InstiMerchantTest {
	
	@Test
	public void reg(){
	
		
		
		try {
			
			
			Map<String, String> formparams=new HashMap<String, String>();
			formparams.put("insCode", "80000303");
			formparams.put("insMerchantCode", "887581298600948");
			formparams.put("merCode", "100130018");
			formparams.put("merName", "彭再锋的商铺1");
			formparams.put("realName", "彭再锋");	
			formparams.put("merState", "湖南省");	
			formparams.put("merCity", "长沙市");	
			formparams.put("merAddress", "暮云街道五矿龙湾国际社区");	
			formparams.put("certType", "01");			
			formparams.put("certId", "43252419870105675x");
			formparams.put("mobile", "15111191925");
			formparams.put("accountId", "6214837210574152");
			formparams.put("accountName", "彭再锋");
			formparams.put("bankName", "招商银行");
			formparams.put("bankCode", "308584000013");			
			formparams.put("openBankName", "");
			formparams.put("openBankState", "");
			formparams.put("openBankCity", "");
			formparams.put("posCertIdImage", "");
			formparams.put("handCertIdImage", "");
			formparams.put("firBuzAreaImage", "");
			formparams.put("secBuzAreaImage", "");
			formparams.put("buzLicenImage", "");
			formparams.put("openPerImage", "");
			formparams.put("operFlag", "A");
			formparams.put("t0drawFee", "0.2");
			formparams.put("t0drawRate", "0.006");
			formparams.put("t1consFee", "0.6");
			formparams.put("t1consRate", "0.006");
			formparams.put("nonceStr", "0.006");
			formparams.put("signature", getSignnatrue(formparams)); 
			
			
			HttpPostUtil util=new HttpPostUtil();
			//util.setIsignoreHttps(true);
			util.initHttp();
			
			String resp=util.post(formparams, "https://gateway.handpay.cn/hpayTransGatewayWeb/instiMerchant/register.htm");
			
			System.out.println(resp);
		}  catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	

	public static String getSignnatrue(Map<String, String> formparams){
		StringBuilder sb=new StringBuilder();
		String signKey="6C6488F20D7072A9C3AAF20798481962";
		sb.append(formparams.get("insCode")).append("|")
				.append(formparams.get("insMerchantCode")).append("|")
				.append(formparams.get("merCode")).append("|")
				.append(formparams.get("merName")).append("|")
				.append(formparams.get("realName")).append("|")
				.append(formparams.get("merState")==null?"":formparams.get("merState")).append("|")
				.append(formparams.get("merCity")==null?"":formparams.get("merCity")).append("|")	
				.append(formparams.get("merAddress")==null?"":formparams.get("merAddress")).append("|")
				.append(formparams.get("certType")).append("|")
				.append(formparams.get("certId")).append("|")
				.append(formparams.get("mobile")).append("|")
				.append(formparams.get("accountId")).append("|")
				.append(formparams.get("accountName")).append("|")
				.append(formparams.get("bankName")).append("|")
				.append(formparams.get("bankCode")).append("|")
				.append(formparams.get("openBankName")==null?"":formparams.get("openBankName")).append("|")
				.append(formparams.get("openBankState")==null?"":formparams.get("openBankState")).append("|")
				.append(formparams.get("openBankCity")==null?"":formparams.get("openBankCity")).append("|")
				.append(formparams.get("operFlag")).append("|")
				.append(formparams.get("t0drawFee")==null?"":formparams.get("t0drawFee")).append("|")
				.append(formparams.get("t0drawRate")==null?"":formparams.get("t0drawRate")).append("|")
				.append(formparams.get("t1consFee")==null?"":formparams.get("t1consFee")).append("|")
				.append(formparams.get("t1consRate")==null?"":formparams.get("t1consRate")).append("|")
				.append(formparams.get("nonceStr")==null?"":formparams.get("t1consRate")).append("|")
				.append(signKey);
				MD5 md5=new MD5();
				System.out.println("md5:"+md5.getMD5ofStr(sb.toString()));
				
				return md5.getMD5ofStr(sb.toString());

	}

}
