package com.hpay.core.common.util;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

import org.apache.log4j.Logger;

import com.hpay.trans.gateway.constant.TransGatewayConstants;
import com.hpay.trans.gateway.exception.HpayChannelException;

public class SocketUtil {
	
	private static final Logger LOG = Logger.getLogger(SocketUtil.class);
	
	public static <T> byte[] sendToBank(String logId, byte[] reqAry,  Class<T> clazz,String ip,Integer port) throws HpayChannelException {
		LOG.info("【ALL:" + logId + "】, [socket]开始...");
		byte[] respAry = null;
		Socket socket = null;
		InputStream is = null;
		OutputStream os = null;
		//1.发送
		try {
			socket = new Socket();
			InetAddress inetAddress = InetAddress.getByName(ip);//211.103.172.37
			InetSocketAddress address = new InetSocketAddress(inetAddress, port);
			socket.setReuseAddress(true);
			socket.connect(address, 3000);
			socket.setSoTimeout(30000);
			os = socket.getOutputStream();
			os.write(reqAry);
		} catch (Exception e) {
			LOG.info("【ALL:" + logId + "】, [socket]请求异常...", e);
			throw new HpayChannelException(TransGatewayConstants.HP_CHANNEL_NET_REQ_ERROR, "网络请求异常"); 
		}
		
		//2.响应
		try {
			while(socket != null && socket.isConnected()){
				is = socket.getInputStream();
				//TODO 可加判断是否为空
				respAry = (byte[]) clazz.getMethod("readResp", InputStream.class).invoke(null, is);
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("【ALL:" + logId + "】, [socket]响应异常...", e);
			throw new HpayChannelException(TransGatewayConstants.HP_CHANNEL_NET_RESP_ERROR, "网络响应异常");
		} finally {
			//3.关闭socket
			try {
				if(socket != null){
					socket.close();
				}
				if(os != null){
					os.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new HpayChannelException(TransGatewayConstants.HP_CHANNEL_SYS_ERROR, "网络IO异常");
			}
		}
		
		LOG.info("【ALL:" + logId + "】, [socket]结束...");
		return respAry;
	}
	
	
	public static <T> byte[] sendToSslBank(byte[] reqAry, Class<T> clazz) throws HpayChannelException {
		//TODO
		return null;
	}

}
