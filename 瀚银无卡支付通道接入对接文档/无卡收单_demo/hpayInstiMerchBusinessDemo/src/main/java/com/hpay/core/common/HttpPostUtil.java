package com.hpay.core.common;

import com.hpay.core.common.BaseHttpSSLSocketFactory.TrustAnyHostnameVerifier;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.hpay.core.common.util.MD5;
import com.hpay.core.common.util.ObjectUtil;



public class HttpPostUtil {
	
	/**
	 * <p>Discription:[日志]</p>
	 */
	private static final Logger logger = Logger.getLogger(HttpPostUtil.class);
	
	/**
	 * <p>Discription:[默认的设置请求超时40秒钟]</p>
	 */
	private static final int DEFAULT_REQUEST_TIMEOUT = 40 * 1000;
	
	/**
	 * <p>Discription:[默认的设置等待数据超时时间40秒钟]</p>
	 */
	private static final int DEFAULT_SO_TIMEOUT = 40 * 1000; 
	
	/**
	 * <p>Discription:[默认的最大连接数200个]</p>
	 */
	private static final int DEFAULT_MAX_TOTAL_SIZE=200; 
	
	/**
	 * <p>Discription:[默认的每个域名最大分配连接数30个]</p>
	 */
	private static final int DEFAULT_MAX_PER_ROUTE_SIZE = 30;
	
	/**
	 * <p>Discription:[设置请求超时40秒钟]</p>
	 */
	public int requestTimeout = DEFAULT_REQUEST_TIMEOUT;
	
	/**
	 * <p>Discription:[设置等待数据超时时间40秒钟]</p>
	 */
	public int sotimeOut = DEFAULT_SO_TIMEOUT; 
	
	/**
	 * <p>Discription:[最大连接数]</p>
	 */
	public int maxTotalSize = DEFAULT_MAX_TOTAL_SIZE;
	
	/**
	 * <p>Discription:[每个域名最大分配连接数]</p>
	 */
	public int maxPerRouteSize = DEFAULT_MAX_PER_ROUTE_SIZE;
	
	/**
	 * <p>Discription:[https keystore证书路径]</p>
	 */
	public String keystoreCertPath;
	
	/**
	 * <p>Discription:[https keystore证书密码]</p>
	 */
	public String keystoreCertPwd;
	
	/**
	 * <p>Discription:[判断是否忽略https，true表示忽略；false表示不忽略，默认值为false]</p>
	 */
	public boolean isignoreHttps = false;
	
	private static RequestConfig defaultRequestConfig = null;
	
	private static PoolingHttpClientConnectionManager connManager = null;
	
	/**
	 * @Description 初始http参数
	 * @version 1.0 
	 * @since 2015年4月20日
	 * @history
	 */
	public void initHttp() throws HttpException {
		logger.info("HttpPostUtil initHttp...");
		// 默认request请求配置
		defaultRequestConfig = RequestConfig.custom().setSocketTimeout(sotimeOut).setConnectTimeout(requestTimeout).setConnectionRequestTimeout(requestTimeout).build();

		SSLConnectionSocketFactory sslsf = null;
		
		// 判断是否忽略https
		if(isignoreHttps){
			sslsf = new SSLConnectionSocketFactory(new BaseHttpSSLSocketFactory(), new TrustAnyHostnameVerifier());
		}else {
			SSLContext sslContext = null;
			try {
				if(ObjectUtil.isNull(keystoreCertPath)){
					
					sslContext = SSLContexts.custom().loadTrustMaterial(null,
			                   new TrustSelfSignedStrategy())
			           .build();
				   HostnameVerifier hostnameVerifier = SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
				    sslsf = new SSLConnectionSocketFactory(
						   sslContext,hostnameVerifier);
				}else{
					sslContext = SSLContexts.custom().loadTrustMaterial(new File(keystoreCertPath), keystoreCertPwd.toCharArray()).build();
					sslsf = new SSLConnectionSocketFactory(sslContext);
				}
			} catch (KeyManagementException e) {
				logger.error("initHttp  exception1:" + e.getMessage(), e);
				throw new HttpException(e.getMessage(), e);
			} catch (NoSuchAlgorithmException e) {
				logger.error("initHttp  exception2:" + e.getMessage(), e);
				throw new HttpException(e.getMessage(), e);
			} catch (KeyStoreException e) {
				logger.error("initHttp  exception3:" + e.getMessage(), e);
				throw new HttpException(e.getMessage(), e);
			} catch (CertificateException e) {
				logger.error("initHttp  exception4:" + e.getMessage(), e);
				throw new HttpException(e.getMessage(), e);
			} catch (IOException e) {
				logger.error("initHttp  exception5:" + e.getMessage(), e);
				throw new HttpException(e.getMessage(), e);
			}
			if (null == sslContext) {
				throw new HttpException("SSL/TSL keystore load error");
			}
			
		}

		Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory> create().
				register("http", PlainConnectionSocketFactory.INSTANCE).
				register("https", sslsf).build();
		
		// 连接池
		connManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
		connManager.setMaxTotal(maxTotalSize);
		connManager.setDefaultMaxPerRoute(maxPerRouteSize);
	}

	/**
	 * @Description 发送http请求
	 * @version 1.0 
	 * @since 2015年4月20日
	 * @param formparams 请求参数
	 * @param sendUrl 请求地址
	 * @return
	 * @throws HttpException
	 * @history
	 */
	public static String post(Map<String, String> formparams, String sendUrl) throws HttpServiceException {
		String message = null;
		CloseableHttpClient httpclient = null;
		CloseableHttpResponse response = null;
		try {
			Integer statusCode = -1;
			HttpPost httpPost = new HttpPost(sendUrl);
			httpPost.setConfig(defaultRequestConfig);
			if (formparams != null && formparams.size() > 0) {
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				for (String key : formparams.keySet()) {
					params.add(new BasicNameValuePair(key, formparams.get(key)));
				}
				httpPost.setEntity(new UrlEncodedFormEntity(params, "utf-8"));
			}
			if (null == connManager) {
				throw new HttpServiceException(ErrorCodesConstant.E_CONNECTOR_HTTP_ERROR, "获取HTTP连接池为空");
			}
			httpclient = HttpClients.custom().setConnectionManager(connManager).setKeepAliveStrategy(new ConnectionKeepAliveStrategy()).build();
			response = httpclient.execute(httpPost);
			Set<HttpRoute> rtz = connManager.getRoutes();

			for (HttpRoute rt : rtz) {
				logger.info("rt=" + rt);
				logger.info("getLeased=" + connManager.getTotalStats().getLeased());
				logger.info("getPending=" + connManager.getTotalStats().getPending());
				logger.info("getAvailable=" + connManager.getTotalStats().getAvailable());
			}

			Header[] headers = response.getAllHeaders();
			for (Header header : headers) {
				logger.info(header);
			}
			statusCode = response.getStatusLine().getStatusCode();
			logger.info("post status:" + statusCode);
			if (statusCode != HttpStatus.SC_OK) {
				logger.error("发送请求失败，Http Status is error");
				throw new HttpServiceException(ErrorCodesConstant.E_CONNECTOR_HTTP_ERROR, "发送请求失败，Http Status is error");
			}
			if (response.getStatusLine().getStatusCode() == 200) {
				HttpEntity resEntity = response.getEntity();
				message = EntityUtils.toString(resEntity, "utf-8");
			}
		} catch (UnsupportedEncodingException e) {
			logger.error("post  exception1:" + e.getMessage(), e);
			throw new HttpServiceException(ErrorCodesConstant.E_CONNECTOR_HTTP_ERROR, "不支持编码异常", e);
		} catch (ClientProtocolException e) {
			logger.error("post  exception2:" + e.getMessage(), e);
			throw new HttpServiceException(ErrorCodesConstant.E_CONNECTOR_HTTP_ERROR, "网络通讯异常", e);
		} catch (HttpHostConnectException e) {
			logger.error("post  exception3:" + e.getMessage(), e);
			throw new HttpServiceException(ErrorCodesConstant.E_CONNECTOR_HTTP_ERROR, "HTTP通讯异常", e);
		} catch (ConnectException e) {
			logger.error("post  exception4:" + e.getMessage(), e);
			throw new HttpServiceException(ErrorCodesConstant.E_CONNECTOR_HTTP_ERROR, "HTTP通讯异常", e);
		} catch (SocketException e) {
			logger.error("post  exception5:" + e.getMessage(), e);
			throw new HttpServiceException(ErrorCodesConstant.E_CONNECTOR_HTTP_ERROR, "HTTP通讯异常", e);
		} catch (ConnectTimeoutException e) {
			logger.error("post  exception6:" + e.getMessage(), e);
			throw new HttpServiceException(ErrorCodesConstant.E_CONNECTOR_HTTP_ERROR, "HTTP请求超时异常", e);
		} catch (SocketTimeoutException e){
			logger.error("post  exception7:" + e.getMessage(), e);
			throw new HttpServiceException(ErrorCodesConstant.E_CONNECTOR_HTTP_RESPONSE_ERROR, "HTTP请求异常", e);
		} catch (HttpServiceException e) {
			logger.error("post  exception8:" + e.getMessage(), e);
			throw new HttpServiceException(e.getErrorCode(), e.getErrorMsg(), e);
		} catch (IOException e) {
			logger.error("post  exception9:" + e.getMessage(), e);
			throw new HttpServiceException(ErrorCodesConstant.E_CONNECTOR_HTTP_REQUEST_ERROR, "HTTP请求异常", e);
		} catch (Exception e) {
			logger.error("post  exception10:" + e.getMessage(), e);
			throw new HttpServiceException(ErrorCodesConstant.E_CONNECTOR_HTTP_REQUEST_ERROR, "HTTP请求异常", e);
		} finally {
			if (response != null) {
				try {
					response.close();
				} catch (IOException e) {
					logger.error("post  exception9:" + e.getMessage(), e);
				}
			}
		}
		return message;
	}
	
	public static int getAvailable(){
		return connManager.getTotalStats().getAvailable();
	}

	public int getRequestTimeout() {
		return requestTimeout;
	}

	public void setRequestTimeout(int requestTimeout) {
		this.requestTimeout = requestTimeout;
	}

	public int getSotimeOut() {
		return sotimeOut;
	}

	public void setSotimeOut(int sotimeOut) {
		this.sotimeOut = sotimeOut;
	}

	public int getMaxTotalSize() {
		return maxTotalSize;
	}

	public void setMaxTotalSize(int maxTotalSize) {
		this.maxTotalSize = maxTotalSize;
	}

	public int getMaxPerRouteSize() {
		return maxPerRouteSize;
	}

	public void setMaxPerRouteSize(int maxPerRouteSize) {
		this.maxPerRouteSize = maxPerRouteSize;
	}

	public String getKeystoreCertPath() {
		return keystoreCertPath;
	}

	public void setKeystoreCertPath(String keystoreCertPath) {
		this.keystoreCertPath = keystoreCertPath;
	}

	public String getKeystoreCertPwd() {
		return keystoreCertPwd;
	}

	public void setKeystoreCertPwd(String keystoreCertPwd) {
		this.keystoreCertPwd = keystoreCertPwd;
	}

	public boolean isIsignoreHttps() {
		return isignoreHttps;
	}

	public void setIsignoreHttps(boolean isignoreHttps) {
		this.isignoreHttps = isignoreHttps;
	}

	
	

	
}
