/*
 * @(#)DateUtils.java        1.0 2009-8-5
 *
 * Copyright (c) 2007-2009 Shanghai Handpay IT, Co., Ltd.
 * 16/F, 889 YanAn Road. W., Shanghai, China
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of 
 * Shanghai Handpay IT Co., Ltd. ("Confidential Information").  
 * You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you 
 * entered into with Handpay.
 */

package com.hpay.core.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;


/**
 * ���ڲ���������
 * 
 * @version 1.0 2009-8-5
 * @author ltang
 * @history
 * 
 */
public class DateUtils {
	private static SimpleDateFormat SDF_YYYYDDMM = new SimpleDateFormat(
			"yyyy-MM-dd");

	/** ���ڸ�ʽyyyyMMddHHmmss **/
	public static final String FORMAT_TIME = "yyyyMMddHHmmss";
	/** ���ڸ�ʽyyyyMMddHHmmssSSS */
	public static final String FORMAT_TIME_2 = "yyyyMMddHHmmssSSS";

	/** ���ڸ�ʽyyyyMMdd **/
	public static final String FORMAT_DATE = "yyyyMMdd";
	/** ���ڸ�ʽyyyyMMddHH **/
	public static final String FORMAT_DATE4 = "yyyyMMddHH";
	/** ʱ���ʽHHmmss **/
	public static final String FORMAT_ONLY_TIME = "HHmmss";
	/** ���ڸ�ʽyyyy/MM/dd **/
	public static final String FORMAT_DATE_1 = "yyyy/MM/dd";
	/** ���ڸ�ʽyyyy��MM��dd�� **/
	public static final String FORMAT_DATE_2 = "yyyy��MM��dd��";

	/** ���ڸ�ʽ YYYY��MM��DD��HHʱMM��SS�� */
	public static final String FORMAT_DATE_3 = "yyyy��MM��dd�� HHʱmm��ss��";

	/** ���ڸ�ʽ yyyy-MM-dd HH:mm:ss **/
	public static final String FORMAT_TIME_SHOW = "yyyy-MM-dd HH:mm:ss";
	/** ���ڸ�ʽ yyyy-MM-dd **/
	public static final String FORMAT_DATE_SHOW = "yyyy-MM-dd";
	
	/** ���ڸ�ʽ HH:mm **/
	public static final String FORMAT_HOUR_SHOW = "HH:mm";
	
	/**  ���ڸ�ʽ HH:mm:ss **/
	public static final String FORMAT_ONLY_TIME_SHOW = "HH:mm:ss";
	
	/** ���ڸ�ʽ HHmm **/
	public static final String FORMAT_HOUR_SHOW_1 = "HHmm";
	
	/** ���ڸ�ʽ YYMM **/
	public static final String FORMAT_DATE_SHORT = "yyMM";
	
	/** ���ڸ�ʽ YYYY **/
	public static final String FORMAT_DATE_YEAR= "yyyy";

	/**
	 * �Ժ���Ϊ��λ
	 */
	public static final long DATE_TIME_BASE = 3600 * 24 * 1000;

	/**
	 * һ��Ϊ��λ
	 */
	public static final long DATE_MINU_BASE = 24 * 60;

	/**
	 * ��yyyyMMddHHmmss��ʽ ��ʽ��Ϊyyyy-MM-dd
	 * 
	 * @param date
	 *            yyyyMMddHHmmss��ʽ
	 * @return yyyy-MM-dd
	 */
	public static String parseToDate(String date) {
		if (ObjectUtil.isNull(date)) {
			return "";
		}
		SimpleDateFormat format = new SimpleDateFormat(FORMAT_TIME);
		SimpleDateFormat fo = new SimpleDateFormat(FORMAT_DATE_SHOW);
		try {
			Date date1 = format.parse(date);
			return fo.format(date1);
		} catch (ParseException e) {
			return "";
		}
	}
	
	public static String getStringForDate() {
		SimpleDateFormat format = new SimpleDateFormat(FORMAT_TIME);
		return format.format(new Date());
		
	}
	
	public static void main(String[] args) {
		System.out.println(getStringForDate());
	}

	/**
	 * ��yyyyMMddHHmmss��ʽ ��ʽ��Ϊyyyy-MM-dd
	 * 
	 * @param date
	 *            yyyyMMddHHmmss��ʽ
	 * @return yyyyMMdd
	 */
	public static String parseToDate3(String date) {
		if (ObjectUtil.isNull(date)) {
			return "";
		}
		SimpleDateFormat format = new SimpleDateFormat(FORMAT_TIME);
		SimpleDateFormat fo = new SimpleDateFormat(FORMAT_DATE);
		try {
			Date date1 = format.parse(date);
			return fo.format(date1);
		} catch (ParseException e) {
			return "";
		}
	}

	/**
	 * ����ָ��ģʽ��ָ�������ַ���
	 * 
	 * @param pattern
	 *            ģʽ��(yyyyMMddHHmmss...)
	 * @param date
	 *            ����
	 * @return ���ͺ󷵻ص����ڶ���
	 */
	public static Date parseDate(String pattern, String date) {
		if (ObjectUtil.isNull(pattern) || ObjectUtil.isNull(date)) {
			return null;
		}

		SimpleDateFormat format = new SimpleDateFormat(pattern);
		try {
			return format.parse(date);
		} catch (ParseException e) {
			Logger.getLogger("DateUtil").warn(e.getMessage(), e);
			return new Date();
		}
	}

	/**
	 * ����ָ��ģʽ��ָ�������ַ���
	 * 
	 * @param pattern
	 *            ģʽ��(yyyyMMddHHmmss...)
	 * @param date
	 *            ����
	 * @return ���ͺ󷵻ص����ڶ���
	 */
	public static String parseDate(String pattern, Date date) {
		if (null == date || ObjectUtil.isNull(pattern)) {
			return null;
		}
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		return format.format(date);
	}

	/**
	 * ��yyyyMMddHHmmss��ʽ ��ʽ��ΪHH:mm:ss
	 * 
	 * @param date
	 *            yyyyMMddHHmmss��ʽ������
	 * @return HH:mm:ss
	 */
	public static String parseToTimeHHmmss(String date) {
		if (ObjectUtil.isNull(date)) {
			return "";
		}
		SimpleDateFormat format = new SimpleDateFormat(FORMAT_TIME);
		SimpleDateFormat fo = new SimpleDateFormat("HH:mm:ss");
		try {
			Date date1 = format.parse(date);
			return fo.format(date1);
		} catch (ParseException e) {
			return "";
		}
	}

	/**
	 * ��yyyyMMddHHmm��ʽ ��ʽ��ΪHH:mm
	 * 
	 * @param date
	 *            yyyyMMddHHmm��ʽ
	 * @return HH:mm
	 */
	public static String parseToTimeHHMM(String date) {
		if (ObjectUtil.isNull(date)) {
			return "";
		}
		SimpleDateFormat format = new SimpleDateFormat(FORMAT_TIME);
		SimpleDateFormat fo = new SimpleDateFormat(FORMAT_HOUR_SHOW);
		try {
			Date date1 = format.parse(date);
			return fo.format(date1);
		} catch (ParseException e) {
			return "";
		}
	}

	/**
	 * ��yyyyMMddHHmmss��ʽ ��ʽ��Ϊyyyy/MM/dd HH:mm:ss
	 * 
	 * @param date
	 *            yyyyMMddHHmmss��ʽ
	 * @return yyyy/MM/dd HH:mm:ss
	 */
	public static String parseToDateYYYYMMDDHHMMSS(String date) {
		if (ObjectUtil.isNull(date)) {
			return "";
		}
		SimpleDateFormat format = new SimpleDateFormat(FORMAT_TIME);
		SimpleDateFormat fo = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		try {
			Date date1 = format.parse(date);
			return fo.format(date1);
		} catch (ParseException e) {
			return "";
		}
	}

	/**
	 * ��yyyyMMddHHmmss��ʽ ��ʽ��Ϊyyyy��MM��dd��
	 * 
	 * @param date
	 *            yyyyMMddHHmmss��ʽ
	 * @return yyyy/MM/dd HH:mm:ss
	 */
	public static String parseToDateYYYYMMDD(String date) {
		if (ObjectUtil.isNull(date)) {
			return "";
		}
		SimpleDateFormat format = null;
		if (date.length() > 8) {
			format = new SimpleDateFormat(FORMAT_TIME);
		} else {
			format = new SimpleDateFormat(FORMAT_DATE);
		}

		SimpleDateFormat fo = new SimpleDateFormat("yyyy��MM��dd��");
		try {
			Date date1 = format.parse(date);
			return fo.format(date1);
		} catch (ParseException e) {
			return "";
		}
	}

	public static String formatDate2YYYYMMDD(Date date) {
		return SDF_YYYYDDMM.format(date);
	}

	/**
	 * ����Ĭ��ʱ���ʽ 14λ:yyyyMMddHHmmss
	 * 
	 * @return
	 */
	public static String getCurrentDate() {
		SimpleDateFormat format = new SimpleDateFormat(FORMAT_TIME);
		return format.format(new Date());
	}

	/**
	 * ����ָ����ʱ���ʽ
	 * 
	 * @return
	 */
	public static String getCurrentDate(String parrten) {
		if (ObjectUtil.isNull(parrten)) {
			return "";
		}
		SimpleDateFormat format = new SimpleDateFormat(parrten);
		return format.format(new Date());
	}

	/**
	 * ���ڱȽϣ�����currDate������false
	 * 
	 * @param format
	 *            �����ַ�����ʽ
	 * @param baseDate
	 *            ���Ƚϵ�����
	 * @param compareDate
	 *            �Ƚ�����
	 * @return
	 */
	public static boolean before(String format, String baseDate,
			String compareDate) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		try {
			Date base = simpleDateFormat.parse(baseDate);
			Date compare = simpleDateFormat.parse(compareDate);

			return compare.before(base);
		} catch (ParseException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}
	
	/**
	 * �Ƚ�Ŀ��ʱ���Ƿ�С�ڵ�ǰʱ��:����currDate������true
	 * 
	 * @author lfjiang 2016��4��21��
	 * @param format
	 * @param targetTime
	 * @return
	 */
	public static boolean afterCurrDate(String format, String targetTime) {
		try {
			return new SimpleDateFormat(format).parse(targetTime).after(
					new Date());
		} catch (ParseException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	/**
	 * ���ݸ�ʽ�õ� stepʱ�����Ժ��ʱ���ַ�����
	 * 
	 * @param format
	 * @param baseTime
	 *            ��׼ʱ��
	 * @param step
	 *            �Է���Ϊ��λ
	 * @return ���صĸ�ʽ Ϊ format
	 */
	public static String getNextTime(String format, String baseTime, long step) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		try {
			Date base = simpleDateFormat.parse(baseTime);
			long nextTime = base.getTime() + step * 60 * 1000;

			Date newTime = new Date();
			newTime.setTime(nextTime);

			return simpleDateFormat.format(newTime);

		} catch (ParseException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	/**
	 * ���ݻ�׼ʱ��,�õ��µ�ʱ��
	 * 
	 * @param baseDate
	 *            ��׼ʱ��
	 * @param step
	 *            ��ʱ����, �Է�Ϊ��λ
	 * @return
	 */
	public static Date getNextTime(Date baseDate, long step) {

		long nextTime = baseDate.getTime() + step * 60 * 1000;

		Date newDate = new Date();
		newDate.setTime(nextTime);

		return newDate;
	}

	/**
	 * ��yyyy-MM-dd��ʽ ��ʽ��ΪyyyyMMdd
	 * 
	 * @param date
	 *            yyyy-MM-dd��ʽ
	 * @return yyyyMMdd
	 */
	public static String parseToDateYYYYMMdd(String date) {
		SimpleDateFormat format = new SimpleDateFormat(FORMAT_DATE_SHOW);
		SimpleDateFormat fo = new SimpleDateFormat(FORMAT_DATE);
		try {
			Date date1 = format.parse(date);
			return fo.format(date1);
		} catch (ParseException e) {
			return "";
		}
	}

	/**
	 * ��yyyyMMdd��ʽ ��ʽ��Ϊyyyy-MM-dd
	 * 
	 * @param date
	 *            yyyyMMdd��ʽ
	 * @return yyyy-MM-dd
	 */
	public static String parseToDate2(String date) {
		SimpleDateFormat format = new SimpleDateFormat(FORMAT_DATE);
		SimpleDateFormat fo = new SimpleDateFormat(FORMAT_DATE_SHOW);
		try {
			Date date1 = format.parse(date);
			return fo.format(date1);
		} catch (ParseException e) {
			return "";
		}
	}

	/**
	 * �����ַ�����ʽת��ͨ�÷���
	 * 
	 * @param date
	 *            Ҫת����ֵ
	 * @param sourceFormate
	 *            Դ���ڸ�ʽ
	 * @param objectiveFormate
	 *            Ŀ�����ڸ�ʽ
	 * @return
	 */
	public static String parseToDate(String date, String sourceFormate,
			String objectiveFormate) {
		SimpleDateFormat format = new SimpleDateFormat(sourceFormate);
		SimpleDateFormat fo = new SimpleDateFormat(objectiveFormate);
		try {
			Date date1 = format.parse(date);
			return fo.format(date1);
		} catch (ParseException e) {
			return "";
		}
	}

	/**
	 * �õ�ǰ�����µ����һ������ں� ǰһ���µ����һ�������(���Ϊ<=0ȡĬ�ϵ�) ������ѡ
	 * 
	 * @param startMonth
	 *            (Ĭ��2)
	 * @param endMonth
	 *            (Ĭ��1)
	 * @param datePattern
	 *            ���ڸ�ʽ(Ĭ��yyyy-MM-dd)
	 * @return key[startDate],key[endDate]
	 */
	public static Map<String, String> getDateForSet(Integer startMonth,
			Integer endMonth, String datePattern) {
		if (startMonth == null) {
			startMonth = -2;
		} else {
			startMonth = Integer.parseInt("-" + startMonth);
		}
		if (endMonth == null) {
			endMonth = -1;
		} else {
			endMonth = Integer.parseInt("-" + startMonth);
		}
		if (ObjectUtil.isNull(datePattern)) {
			datePattern = FORMAT_DATE_SHOW;
		}

		SimpleDateFormat format = new SimpleDateFormat(datePattern);
		Calendar start = Calendar.getInstance();
		start.add(Calendar.MONTH, startMonth);
		start.set(Calendar.DAY_OF_MONTH, start
				.getActualMaximum(Calendar.DAY_OF_MONTH));
		String startDate = format.format(start.getTime());

		Calendar end = Calendar.getInstance();
		end.add(Calendar.MONTH, endMonth);
		end.set(Calendar.DAY_OF_MONTH, end
				.getActualMaximum(Calendar.DAY_OF_MONTH));
		String endDate = format.format(end.getTime());
		Map<String, String> rs = new HashMap<String, String>(1);
		rs.put("startDate", startDate);
		rs.put("endDate", endDate);
		return rs;
	}

	/**
	 * ��yyyyMMddHHmmss��ʽ ��ʽ��Ϊyyyy-MM-dd HH:mm:ss
	 * 
	 * @param date
	 *            yyyyMMddHHmmss��ʽ
	 * @return yyyy/MM/dd HH:mm:ss
	 */
	public static String parseToDateAD(String date) {
		if (ObjectUtil.isNull(date)) {
			return "";
		}
		SimpleDateFormat format = new SimpleDateFormat(FORMAT_TIME);
		SimpleDateFormat fo = new SimpleDateFormat(FORMAT_TIME_SHOW);
		try {
			Date date1 = format.parse(date);
			return fo.format(date1);
		} catch (ParseException e) {
			return "";
		}
	}

	/**
	 * ��yyyy-MM-dd HH:mm:ss��ʽ ��ʽת��ΪyyyyMMddHHmmss
	 * 
	 * @param FORMAT_TIME
	 * @param date
	 * @return
	 */
	public static String parseToDateS(String date) {
		if (ObjectUtil.isNull(date)) {
			return null;
		}

		SimpleDateFormat format1 = new SimpleDateFormat(FORMAT_TIME_SHOW);
		SimpleDateFormat format2 = new SimpleDateFormat(FORMAT_TIME);

		try {
			Date date1 = format1.parse(date);
			return format2.format(date1);
		} catch (ParseException e) {
			return null;
		}

	}

	/**
	 * ��yyyyMMddHHmmss��ʽ ��ʽ��ΪYYYY��MM��DD��HHʱMM��SS��
	 * 
	 * @param date
	 *            yyyyMMddHHmmss��ʽ
	 * @return yyyy/MM/dd HH:mm:ss
	 */
	public static String parseToDateAD3(String date) {
		if (ObjectUtil.isNull(date)) {
			return "";
		}
		SimpleDateFormat format = new SimpleDateFormat(FORMAT_TIME);
		SimpleDateFormat fo = new SimpleDateFormat(FORMAT_DATE_3);
		try {
			Date date1 = format.parse(date);
			return fo.format(date1);
		} catch (ParseException e) {
			return "";
		}
	}

	/**
	 * ȡ��ǰʱ��ǰһ��͵�ǰʱ��
	 * 
	 */
	public static Map<String, String> getLastCurrDay() {
		String datePattern = "yyyy-MM-dd";
		SimpleDateFormat format = new SimpleDateFormat(datePattern);
		Calendar calendarStart = Calendar.getInstance();
		calendarStart.add(Calendar.DATE, -1); // �õ�ǰһ��
		String startDate = format.format(calendarStart.getTime());
		Calendar calendarEnd = Calendar.getInstance();
		calendarEnd.add(Calendar.DATE, 0); // �õ�ǰһ��
		String endDate = format.format(calendarEnd.getTime());
		Map<String, String> rs = new HashMap<String, String>(1);
		rs.put("startDate", startDate);
		rs.put("endDate", endDate);
		return rs;
	}

	/**
	 * ȡ���������ڵļ������ Ĭ�������ʽΪ(yyyyMMdd)
	 * 
	 * @param originalDate
	 * @param intervalDay
	 * @return
	 */
	public static String getIntervalDay(String originalDate, int intervalNumber) {
		Date currentDate = DateUtils.parseDate(FORMAT_DATE, originalDate);
		Date tempIntervalDay = DateUtils.addDays(currentDate, intervalNumber);
		return DateUtils.parseDate(FORMAT_DATE, tempIntervalDay);

	}

	/**
	 * ȡ���µ�һ��͵�ǰʱ��
	 * 
	 */
	public static Map<String, String> getFirstDayInMonth() {
		String datePattern = FORMAT_DATE_SHOW;
		SimpleDateFormat format = new SimpleDateFormat(datePattern);
		Calendar calendarStart = Calendar.getInstance();
		calendarStart.set(Calendar.DAY_OF_MONTH, 1); // �õ����µ�һ��
		String startDate = format.format(calendarStart.getTime());
		Calendar calendarEnd = Calendar.getInstance();
		calendarEnd.add(Calendar.DATE, 0); // �õ���������
		String endDate = format.format(calendarEnd.getTime());
		Map<String, String> rs = new HashMap<String, String>(1);
		rs.put("startDate", startDate);
		rs.put("endDate", endDate);
		return rs;
	}

	/**
	 * ȡ���µ�һ��͵�ǰʱ��
	 * 
	 */
	public static Map<String, String> getFirstDayInMonth(String datePattern) {
		SimpleDateFormat format = new SimpleDateFormat(datePattern);
		Calendar calendarStart = Calendar.getInstance();
		calendarStart.set(Calendar.DAY_OF_MONTH, 1); // �õ����µ�һ��
		String startDate = format.format(calendarStart.getTime());
		Calendar calendarEnd = Calendar.getInstance();
		calendarEnd.add(Calendar.DATE, 0); // �õ���������
		String endDate = format.format(calendarEnd.getTime());
		Map<String, String> rs = new HashMap<String, String>(1);
		rs.put("startDate", startDate);
		rs.put("endDate", endDate);
		return rs;
	}

	/**
	 * �Ƚ��������ڵ�˳��
	 * 
	 * @param format�����ַ�����ʽ
	 * @param baseDate���Ƚϵ�����
	 * @param compareDate�Ƚ�����
	 * @return ������� compareDate ���ڴ� baseDate���򷵻�ֵ 0������� compareDate ��baseDate
	 *         ����֮ǰ���򷵻ش��� 0 ��ֵ������� compareDate�� baseDate ����֮���򷵻�С�� 0 ��ֵ��
	 */
	public static int compareTo(String format, String compareDate,
			String baseDate) {

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		try {
			Date base = simpleDateFormat.parse(baseDate);
			Date compare = simpleDateFormat.parse(compareDate);
			return base.compareTo(compare);
		} catch (ParseException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	/**
	 * Ϊ������������
	 * 
	 * @param curDateType
	 * @param days
	 * @return
	 */
	public static Date addDays(Date curDateType, int days) {
		Calendar expiration = Calendar.getInstance();
		expiration.setTime(curDateType);
		expiration.add(Calendar.DAY_OF_MONTH, days);

		return expiration.getTime();
	}

	/**
	 * Ϊ������������
	 * 
	 * @param curDateType
	 * @param expTime
	 * @return
	 */
	public static Date addMonths(Date curDateType, int expTime) {
		Calendar expiration = Calendar.getInstance();
		expiration.setTime(curDateType);
		expiration.add(Calendar.MONTH, expTime);

		return expiration.getTime();
	}

	/**
	 * ��������������������
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static long getDistDates(Date startDate, Date endDate) {
		long totalDate = 0;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);

		long timestart = calendar.getTimeInMillis();
		calendar.setTime(endDate);
		long timeend = calendar.getTimeInMillis();

		totalDate = (timeend - timestart) / (1000 * 60 * 60 * 24);
		return totalDate;
	}

	/**
	 * ����YYYY-MM-DD�������ַ�������,��������������������
	 * 
	 * @param start
	 * @param end
	 * @return
	 * @throws ParseException
	 */
	public static long getDistDates(String start, String end)
			throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DATE_SHOW);
		Date startDate = sdf.parse(start);
		Date endDate = sdf.parse(end);
		return getDistDates(startDate, endDate);
	}

	/**
	 * ����������
	 * 
	 * @param beginDate
	 * @param endDate
	 * @return
	 * @throws ParseException
	 */
	public static long getDiffDay(String beginDate, String endDate) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat(FORMAT_DATE_SHOW);
		Date dbeginDate = null;
		Date dendDate = null;
		dbeginDate = formatter.parse(beginDate);
		dendDate = formatter.parse(endDate);
		long daysBetween=(dendDate.getTime()-dbeginDate.getTime()+1000000L)/(3600*24*1000);
		return daysBetween;
	}
	
	/**
	 * ��������������������
	 * @author lfjiang 2014��7��22��
	 * @param start[��ʼ����]
	 * @param startPattern[��ʼ���ڸ�ʽ]
	 * @param end[��������]
	 * @param endPattern[�������ڸ�ʽ]
	 * @return
	 * @throws ParseException
	 */
	public static long getDistDays(String start , String startPattern , String end , String endPattern) throws ParseException {
		SimpleDateFormat startSdf = new SimpleDateFormat(startPattern);
		SimpleDateFormat endSdf = new SimpleDateFormat(endPattern);
		return getDistDates(startSdf.parse(start), endSdf.parse(end));
	}

	/**
	 * ����YYYY-MM-DD�������ַ�������,��������������������
	 * 
	 * @param ��beginDate
	 * @param ��endDate
	 * @return �������
	 * @throws ParseException
	 */
	public static int getDiffMonth(String beginDate, String endDate)
			throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat(FORMAT_DATE_SHOW);
		Date dbeginDate = null;
		Date dendDate = null;
		dbeginDate = formatter.parse(beginDate);
		dendDate = formatter.parse(endDate);
		return getDiffMonth(dbeginDate, dendDate);
	}

	/**
	 * ��������������������
	 * 
	 * @param beginDate
	 * @param endDate
	 * @return �������
	 */
	public static int getDiffMonth(Date beginDate, Date endDate) {
		Calendar calbegin = Calendar.getInstance();
		Calendar calend = Calendar.getInstance();
		calbegin.setTime(beginDate);
		calend.setTime(endDate);
		int mBegin = calbegin.get(Calendar.MONTH) + 1; // ��ú�ͬ��ʼ�����·�
		int mEnd = calend.get(Calendar.MONTH) + 1;
		// ��ú�ͬ���������·�
		int checkmonth = mEnd - mBegin
				+ (calend.get(Calendar.YEAR) - calbegin.get(Calendar.YEAR))
				* 12;
		// ��ú�ͬ���������ڿ�ʼ������·�
		return checkmonth;
	}

	/**
	 * ��ҳ���ϵ�ʱ���ʽ yyyy-MM-dd ���� yyyy-MM-dd HH:mm:ss ת��Ϊ���ʱ���ʽ yyyyMMdd ����
	 * yyyyMMddHHmmss
	 * 
	 * @param date
	 *            2012/08/01 author lysun
	 * @return
	 * @throws ParseException
	 */
	public static String getHpDate(String date) {
		String result = null;
		try {
			if (ObjectUtil.isNull(date)) {
				result = "";
			} else {
				result = new SimpleDateFormat(FORMAT_TIME)
						.format(new SimpleDateFormat("yyyy-MM-dd H:m:s")
								.parse(date));
			}
		} catch (Exception e) {
			result = date.replace("-", "");
		}
		return result;
	}

	/**
	 * �����ʱ���ʽת����ҳ����ʾ��ʱ���ʽ yyyyMMdd ���� yyyyMMddHHmmss ת��Ϊ yyyy-MM-dd ����
	 * yyyy-MM-dd HH:mm:ss
	 * 
	 * @param date
	 *            2012/08/01 author lysun
	 * @return
	 */
	public static String getDateFromHpDate(String date) {
		String result = null;
		if (ObjectUtil.isNull(date)) {
			result = "";
		} else {
			if (8 == date.length()) {
				result = DateUtils.parseToDate2(date);
			}
			if (14 == date.length()) {
				result = DateUtils.parseToDateAD(date);
			}
		}
		return result;
	}

	/**
	 * ����yyyyMMddHHmmss�������ַ�������,���������������ĺ�����
	 * 
	 * @param start
	 * @param end
	 * @return
	 * @throws ParseException
	 */
	public static long getDistTimeInMillis(String start, String end)
			throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_TIME);
		Date startDate = sdf.parse(start);
		Date endDate = sdf.parse(end);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);

		long timestart = calendar.getTimeInMillis();
		calendar.setTime(endDate);
		long timeend = calendar.getTimeInMillis();

		return (timeend - timestart);
	}

	/**
	 * ��ȡ��������֮�������(�������˵�����)
	 * 
	 * @param beginDate
	 *            ��ʼ����(��ʽ:yyyyMMdd)
	 * @param endDate
	 *            �ս�����(��ʽ:yyyyMMdd) ���beginDate����endDate,�򻥻�ֵ
	 * @return
	 */
	public static List<String> getBetweenDates(String beginDate, String endDate) {
		String temp = "";
		if (compareTo(FORMAT_DATE, beginDate, endDate) < 0) {
			temp = beginDate;
			beginDate = endDate;
			endDate = temp;
		}

		long tmpTime = parseDate(FORMAT_DATE, beginDate).getTime();
		long endTime = parseDate(FORMAT_DATE, endDate).getTime();
		List<String> result = new ArrayList<String>();
		while (tmpTime <= endTime) {
			Date targetDate = new Date(tmpTime);
			result.add(parseDate(FORMAT_DATE, targetDate));
			tmpTime += DATE_TIME_BASE;
		}
		return result;
	}


	
	/**
	 * �ж�ĳһ���Ƿ�����ĩ
	 * 
	 * @param datePattern
	 *            ��ʽyyyyMMddHHmmss
	 * @return
	 */
	public static boolean isMonthLastDay(String datePattern) {
		Date before = parseDate(FORMAT_TIME, datePattern);
		Calendar beforeCal = Calendar.getInstance();// ��������
		beforeCal.setTime(before);
		final int beforeMonth = beforeCal.get(Calendar.MONTH);

		beforeCal.add(Calendar.DAY_OF_MONTH, 1);
		final int afterMonth = beforeCal.get(Calendar.MONTH);
		return !(beforeMonth == afterMonth);
	}

	@SuppressWarnings("deprecation")
	public int getDutyDays(String strStartDate, String strEndDate) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate = null;
		Date endDate = null;
		try {
			startDate = df.parse(strStartDate);
			endDate = df.parse(strEndDate);
		} catch (ParseException e) {
		}

		int result = 0;
		while (null != startDate && null != endDate && startDate.compareTo(endDate) <= 0) {
			if (startDate.getDay() != 6 && startDate.getDay() != 0) {
				result++;
			}
			startDate.setDate(startDate.getDate() + 1);
		}

		return result;
	}

	/**
	 * �õ���ǰ���ڵ���һ��N��
	 * 
	 * @param formatPattern
	 *            �������ֵΪ�գ���Ĭ��ΪYYYYMMDD
	 * @param inputDate
	 * @param stepDay
	 *            �������
	 * @return ���ظ�ʽͬformatPatternһ��
	 */
	public static String getNextNDay(String formatPattern,String inputDate,int stepDay)
	{
		if(formatPattern==null||formatPattern.trim().length()==0)
		{
			formatPattern = DateUtils.FORMAT_DATE;
		}
		Date nextBeginDay= DateUtils.addDays(DateUtils.parseDate(formatPattern,inputDate),stepDay);
		SimpleDateFormat dateFormat= new SimpleDateFormat(formatPattern);
		return dateFormat.format(nextBeginDay);
	}

	/**
	 * ��ȡ��ǰ����
	 * 
	 * @return
	 */
	public static String getCurrentDay() {
		String currentDay = null;
		SimpleDateFormat format = new SimpleDateFormat(FORMAT_TIME);
		currentDay = format.format(new Date());
		return currentDay;
	}

	public static String getNextDay() {
		Calendar c = Calendar.getInstance();// ���Զ�ÿ��ʱ���򵥶��޸�
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int date = c.get(Calendar.MONDAY);
		int hour = c.get(Calendar.HOUR_OF_DAY);
		int minute = c.get(Calendar.MINUTE);
		int second = c.get(Calendar.SECOND);
		String nextYear = year - 1 + "" + month + "" + date + "" + hour + ""
				+ minute + "" + second;
		return nextYear;
	}

	public static Date getTAddTwoDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int count = 0;
		while (count != 2) {

			if (cal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY
					&& cal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
				cal.add(Calendar.DAY_OF_MONTH, 1);
				count++;
			} else {
				cal.add(Calendar.DAY_OF_MONTH, 1);
			}

			// if(cal.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY ||
			// cal.get(Calendar.DAY_OF_WEEK)==Calendar.SUNDAY){
			// cal.add(Calendar.DAY_OF_MONTH, 1);
			// count ++;
			// }else{
			// cal.add(Calendar.DAY_OF_MONTH, 1);
			// }
		}

		return cal.getTime();

	}

	/**
	 * ���ݵ�ǰ���ڼ�����interval��������(Ĭ�ϸ�ʽ��"yyyy-MM-dd"
	 * 
	 * @param interval
	 * @return
	 */
	public static String calcDateOfInterval(int interval) {
		SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
		Date beginDate = new Date();
		Calendar date = Calendar.getInstance();
		date.setTime(beginDate);
		date.set(Calendar.DATE, date.get(Calendar.DATE) + interval);
		Date endDate = null;
		try {
			endDate = dft.parse(dft.format(date.getTime()));
		} catch (ParseException e) {
			System.out.println("�Ƿ������ڸ�ʽ,�޷�����ת��");
		}
		return parseDate("yyyy-MM-dd", endDate);
	}

	public static String getDayOfWee(String formatPattern){
		Calendar cal =Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat(formatPattern);//�������ڸ�ʽ
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY); 
        return df.format(cal.getTime());
	}

	/**
	 * ������ڣ�yyyyMMdd
	 * 
	 * @param value
	 * @param datePattern
	 * @return
	 */
	public static boolean isDate(String value) {
		String str = "^(?:(?!0000)[0-9]{4}([-/.]?)(?:(?:0?[1-9]|1[0-2])([-/.]?)(?:0?[1-9]|1[0-9]|2[0-8])|(?:0?[13-9]|1[0-2])([-/.]?)(?:29|30)|(?:0?[13578]|1[02])([-/.]?)31)|(?:[0-9]{2}(?:0[48]|[2468][048]|[13579][26])|(?:0[48]|[2468][048]|[13579][26])00)([-/.]?)0?2([-/.]?)29)$";
		Pattern p = Pattern.compile(str);
		String s = value.replaceAll("-", "/");
		return p.matcher(s).matches();
	}

	/**
	 * ���ʱ�䣺HHmmss
	 * 
	 * @param value
	 * @param datePattern
	 * @return
	 */
	public static boolean isTime(String value) {
		String str = "([01][0-9]|2[0-3])[0-5][0-9][0-5][0-9]";
		Pattern p = Pattern.compile(str);
		String s = value.replaceAll("-", "/");
		return p.matcher(s).matches();
	}

	public static String ystDay() {
		return DateUtils.formatDate2YYYYMMDD(DateUtils.addDays(new Date(), -1));
	}
	
	/**
	 * @Description ��  ����ʱ�䣬�Ƴ��������
	 * @creatime 2015��12��5�� ����2:41:35 
	 * @param time ʱ�䣬��ʽ��MMddHHmmss �� MMdd
	 * @return
	 */
	public static String getYearByFormatTime(String time) {
		if (ObjectUtil.isNull(time)) {
			return "";
		}
		Calendar cal     = Calendar.getInstance();
		int currentYear  = cal.get(Calendar.YEAR);
		int currentMonth = cal.get(Calendar.MONTH)+ 1;
		int currentYearAndMonth = currentYear + currentMonth;

		String month 	 = time.substring(0, 2);
		int yearAndMonth = currentYear + Integer.valueOf(month).intValue();
		
		if (currentYearAndMonth >= yearAndMonth) {
			return currentYear + time;
		}
		return (currentYear - 1) + time;
	}
	
	/**
	 * Ϊ�������ӷ���
	 * 
	 * @param curDateType
	 * @param minute
	 * @return
	 */
	public static Date addMinute(Date curDateType, int minute) {
		Calendar expiration = Calendar.getInstance();
		expiration.setTime(curDateType);
		expiration.add(Calendar.MINUTE, minute);

		return expiration.getTime();
	}
	
	/** 
     * ������Ӳ�
     */  
	public static long getDiffMinute(String beginDate, String endDate, String pattern) {
		SimpleDateFormat df = new SimpleDateFormat(pattern);
		long min = 0;
		try {
			Date one = df.parse(beginDate);
			Date two = df.parse(endDate);
			long time1 = one.getTime();
			long time2 = two.getTime();
			long diff;
			if (time1 < time2) {
				diff = time2 - time1;
			} else {
				diff = time1 - time2;
			}
			min = (diff / (60 * 1000));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return min;
	}  
	
}
