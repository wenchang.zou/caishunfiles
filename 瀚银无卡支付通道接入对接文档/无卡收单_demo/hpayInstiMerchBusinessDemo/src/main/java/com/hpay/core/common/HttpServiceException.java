/*
 * @(#)HttpServiceException.java        1.0 2015年9月11日
 *
 * Copyright (c) 2007-2014 Shanghai Handpay IT, Co., Ltd.
 * No. 80 Xinchang Rd, Huangpu District, Shanghai, China
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of 
 * Shanghai Handpay IT Co., Ltd. ("Confidential Information").  
 * You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you 
 * entered into with Handpay.
 */
package com.hpay.core.common;


/**
 * @Description http服务异常类
 * @version 1.0
 * @author yhe
 * @since 2015年9月11日
 * @history 
 * 时间 版本 姓名 修改内容
 */
public class HttpServiceException extends BaseException {

	/** serialVersionUID */
	private static final long serialVersionUID = 3329595588421120758L;

	public HttpServiceException() {
	}

	public HttpServiceException(String errorCode, String errorMsg) {
		super(errorCode, errorMsg);
	}

	public HttpServiceException(String errorCode, Throwable caused) {
		super(errorCode, caused);
	}

	public HttpServiceException(String errorCode, String errorMsg, Throwable caused) {
		super(errorCode, errorMsg, caused);
	}
}
