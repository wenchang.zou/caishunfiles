/*
 * @(#)TransGatewayConstants.java    1.0.0 2016年11月9日 下午3:13:26
 *
 * Copyright (c) 2007-2016 Shanghai Handpay IT, Co., Ltd. 
 * 4/F, 763 MengZi Road. W., Shanghai, China 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of 
 * Shanghai Handpay IT Co., Ltd. ("Confidential Information").  
 * You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you 
 * entered into with Handpay.
 */
package com.hpay.trans.gateway.constant;

/**
 * 支付网关常量类
 * 
 * @version 1.0.0 2016年11月9日 下午3:13:26
 * @author yhe
 * @history 
 *			时间	版本	姓名	修改内容
 */
public class TransGatewayConstants {

	/** #号 */
	public static final String SYMBOL_DELIMITER = "#";
	/** 空 */
	public static final String SYMBOL_BLANK = "";
	/** (号 */
	public static final String SYMBOL_LEFTBRACKET = "(";
	/** )号 */
	public static final String SYMBOL_RIGHTBRACKET = ")";
	/** 网关基础数据状态，00有效 */
	public static final String GATEWAY_BASE_DATA_VALID = "00";
	/** 网关基础数据状态，01无效 */
	public static final String GATEWAY_BASE_DATA_INVALID = "01";
	/** 缓存默认的过期时间 */
	public static final int DEFAULT_CACHE_TIMEOUT = 86400;
	/** 版本号 */
	public static final String VERSION = "1.0.0";
	/** 币种：人民币 */
	public static final String CURRENCY = "CNY";
	/** 币种：156 */
	public static final String CURRENCY_CODE = "156";
	/** 编码：UTF-8 */
	public static final String ENCODING_UTF_8 = "UTF-8";
	/** 编码：GBK */
	public static final String ENCODING_GBK = "GBK";
	/** 签名方式：RSA */
	public static final String SIGN_METHOD = "RSA";
	/** 状态：00成功 */
	public static final String TRANS_STATUS_SUCC = "00";
	/** 默认网关：9999 */
	public static final String GATEWAYNUM = "9999";
	/** 通道-返回码前缀 */
	public static final String HP_CHANNEL = "HP_CHANNEL_";
	/** 通道系统异常 */
	public static final String HP_CHANNEL_SYS_ERROR = HP_CHANNEL + "Z0";
	/** 网络请求异常 */
	public static final String HP_CHANNEL_NET_REQ_ERROR = HP_CHANNEL + "Z1";
	/** 网络响应异常 */
	public static final String HP_CHANNEL_NET_RESP_ERROR = HP_CHANNEL + "Z2";
	/**费率百分比规则*/
	public static final String REGEX_RATE="^(([0]{1}))(\\.(\\d){0,6})?$";
	/**费率固定金额规则*/
	public static final String REGEX_FEE="^(([0-9]{1,4}))(\\.(\\d){0,4})?$";
	/**交易金额（分）格式规则*/
	public static final String REGEX_TRANS_AMOUNT="^(([0-9]{1,10}))?$";
	/** 出资方中间账户ID */
	public static final String ACCOUNTID = "ACCOUNTID";

}
