package com.hpay.core.common.util;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;

/**
 * 安全工具
 * @author jhjiang
 *
 */
public class SecureUtil {
	
	private static final Logger LOG = Logger.getLogger(SecureUtil.class);

	public static final String KEY_ALGORITHM_DES = "DES";			// DES-密钥算法
	
	public static final String KEY_ALGORITHM_3DES = "DESede";		// 3DES-密钥算法
	
	public static final String KEY_ALGORITHM_RSA = "RSA";			// RSA-密钥算法
	
	
	/**
	 * MD5加密-数组
	 * @param dataByteAry	待加密数据-byte数组
	 * @return
	 */
	public static byte[] enByMD5(byte[] dataByteAry){
		return DigestUtils.md5(dataByteAry);
	}
	
	/**
	 * DES加密-数组
	 * @param dataAry	待加密数据字节数组	
	 * @param keyAry	密钥字节数组-8个字节
	 * @param algorithm 算法
	 * @return
	 * @throws Exception
	 */
	public static byte[] enByDES(byte[] dataAry, byte[] keyAry, String algorithm) {
		try {
			DESKeySpec keySpec = new DESKeySpec(keyAry);									// 1.根据key实例化规范
			SecretKeyFactory factory = SecretKeyFactory.getInstance(KEY_ALGORITHM_DES);		// 2.实例化密钥工厂 - DES算法
			SecretKey key = factory.generateSecret(keySpec);								// 3.根据规范，还原密钥
			Cipher c = Cipher.getInstance(algorithm);										// 4.实例化
			c.init(Cipher.ENCRYPT_MODE, key);												// 5.初始化，设置为加密模式
			return c.doFinal(dataAry);														// 6.执行操作
		} catch (InvalidKeySpecException e) {
			LOG.error("DES加密发生异常，无效的密钥规范", e);
		} catch (InvalidKeyException e) {
			LOG.error("DES加密发生异常，无效的密钥", e);
		} catch (NoSuchAlgorithmException e) {
			LOG.error("DES加密发生异常，没有[" + algorithm + "]算法", e);
		} catch (NoSuchPaddingException e) {
			LOG.error("DES加密发生异常，无效的填充方法", e);
		} catch (IllegalBlockSizeException e) {
			LOG.error("DES加密发生异常，非法的块长度", e);
		} catch (BadPaddingException e) {
			LOG.error("DES加密发生异常，错误的填充方法", e);
		} 
		return null;
	}
	
	
	/**
	 * DES解密-数组
	 * @param dataAry	待解密数据字节数组	
	 * @param keyAry	密钥字节数组
	 * @param algorithm 算法
	 * @return
	 * @throws Exception
	 */
	public static byte[] deByDES(byte[] dataAry, byte[] keyAry, String algorithm) {
		try {
			DESKeySpec keySpec = new DESKeySpec(keyAry);									// 1.根据key实例化规范
			SecretKeyFactory factory = SecretKeyFactory.getInstance(KEY_ALGORITHM_DES);		// 2.实例化密钥工厂 - DES算法
			SecretKey key = factory.generateSecret(keySpec);								// 3.根据规范，还原密钥
			Cipher c = Cipher.getInstance(algorithm);										// 4.实例化
			c.init(Cipher.DECRYPT_MODE, key);												// 5.初始化，设置为解密模式
			return c.doFinal(dataAry);														// 6.执行操作
		} catch (InvalidKeySpecException e) {
			LOG.error("DES解密发生异常，无效的密钥规范", e);
		} catch (InvalidKeyException e) {
			LOG.error("DES解密发生异常，无效的密钥", e);
		} catch (NoSuchAlgorithmException e) {
			LOG.error("DES解密发生异常，没有[" + algorithm + "]算法", e);
		} catch (NoSuchPaddingException e) {
			LOG.error("DES解密发生异常，无效的填充方法", e);
		} catch (IllegalBlockSizeException e) {
			LOG.error("DES解密发生异常，非法的块长度", e);
		} catch (BadPaddingException e) {
			LOG.error("DES解密发生异常，错误的填充方法", e);
		} 
		return null;
	}
	
	/**
	 * 3DES加密-数组
	 * @param dataAry	待加密数据字节数组	
	 * @param keyAry	密钥字节数组
	 * @param algorithm 算法
	 * @return
	 * @throws Exception
	 */
	public static byte[] enBy3DES(byte[] dataAry, byte[] keyAry, String algorithm) {
		try {
			DESedeKeySpec keySpec = new DESedeKeySpec(keyAry);								// 1.根据key实例化规范
			SecretKeyFactory factory = SecretKeyFactory.getInstance(KEY_ALGORITHM_3DES);	// 2.实例化密钥工厂 - DESede算法
			SecretKey key = factory.generateSecret(keySpec);								// 3.根据规范，还原密钥
			Cipher c = Cipher.getInstance(algorithm);										// 4.实例化
			c.init(Cipher.ENCRYPT_MODE, key);												// 5.初始化，设置为加密模式
			return c.doFinal(dataAry);														// 6.执行操作
		} catch (InvalidKeySpecException e) {
			LOG.error("3DES加密发生异常，无效的密钥规范", e);
		} catch (InvalidKeyException e) {
			LOG.error("3DES加密发生异常，无效的密钥", e);
		} catch (NoSuchAlgorithmException e) {
			LOG.error("3DES加密发生异常，没有[" + algorithm + "]算法", e);
		} catch (NoSuchPaddingException e) {
			LOG.error("3DES加密发生异常，无效的填充方法", e);
		} catch (IllegalBlockSizeException e) {
			LOG.error("3DES加密发生异常，非法的块长度", e);
		} catch (BadPaddingException e) {
			LOG.error("3DES加密发生异常，错误的填充方法", e);
		} 
		return null;
	}

	/**
	 * 3DES解密-数组
	 * @param dataAry	待解密数据字节数组	
	 * @param keyAry	密钥字节数组
	 * @param algorithm 算法
	 * @return
	 * @throws Exception
	 */
	public static byte[] deBy3DES(byte[] dataAry, byte[] keyAry, String algorithm) {
		try {
			DESedeKeySpec keySpec = new DESedeKeySpec(keyAry);								// 1.根据key实例化规范
			SecretKeyFactory factory = SecretKeyFactory.getInstance(KEY_ALGORITHM_3DES);	// 2.实例化密钥工厂 - DESede算法
			SecretKey key = factory.generateSecret(keySpec);								// 3.根据规范，还原密钥
			Cipher c = Cipher.getInstance(algorithm);										// 4.实例化
			c.init(Cipher.DECRYPT_MODE, key);												// 5.初始化，设置为解密模式
			return c.doFinal(dataAry);														// 6.执行操作
		} catch (InvalidKeySpecException e) {
			LOG.error("3DES解密发生异常，无效的密钥规范", e);
		} catch (InvalidKeyException e) {
			LOG.error("3DES解密发生异常，无效的密钥", e);
		} catch (NoSuchAlgorithmException e) {
			LOG.error("3DES解密发生异常，没有[" + algorithm + "]算法", e);
		} catch (NoSuchPaddingException e) {
			LOG.error("3DES解密发生异常，无效的填充方法", e);
		} catch (IllegalBlockSizeException e) {
			LOG.error("3DES解密发生异常，非法的块长度", e);
		} catch (BadPaddingException e) {
			LOG.error("3DES解密发生异常，错误的填充方法", e);
		} 
		return null;
	}
	
	/**
	 * RSA加密-数组
	 * @param dataAry	待加密数组
	 * @param keyAry	密钥数组
	 * @param algorithm	算法
	 * @param keyType	加密类型-{false：公钥加密，true: 私钥加密}
	 * @return
	 */
	public static byte[] enByRSA(byte[] dataAry, byte[] keyAry, String algorithm, boolean keyType){
		try {
			EncodedKeySpec keySpec = keyType ? new PKCS8EncodedKeySpec(keyAry) : new X509EncodedKeySpec(keyAry);
			KeyFactory factory = KeyFactory.getInstance(KEY_ALGORITHM_RSA);
			Key key = keyType ? factory.generatePrivate(keySpec) : factory.generatePublic(keySpec);
			Cipher c = Cipher.getInstance(algorithm);
			c.init(Cipher.ENCRYPT_MODE, key);
			return c.doFinal(dataAry);
		} catch (InvalidKeySpecException e) {
			LOG.error("RSA加密发生异常，无效的密钥规范", e);
		} catch (InvalidKeyException e) {
			LOG.error("RSA加密发生异常，无效的密钥", e);
		} catch (NoSuchAlgorithmException e) {
			LOG.error("RSA加密发生异常，没有[" + algorithm + "]算法", e);
		} catch (NoSuchPaddingException e) {
			LOG.error("RSA加密发生异常，无效的填充方法", e);
		} catch (IllegalBlockSizeException e) {
			LOG.error("RSA加密发生异常，非法的块长度", e);
		} catch (BadPaddingException e) {
			LOG.error("RSA加密发生异常，错误的填充方法", e);
		} 
		return null;
	}
	
	/**
	 * RSA解密-数组
	 * @param dataAry	待解密数组
	 * @param keyAry	密钥数组
	 * @param algorithm	算法
	 * @param keyType	解密类型-{false：公钥解密，true: 私钥解密}
	 * @return
	 */
	public static byte[] deByRSA(byte[] dataAry, byte[] keyAry, String algorithm, boolean keyType){
		try {
			EncodedKeySpec keySpec = keyType ? new PKCS8EncodedKeySpec(keyAry) : new X509EncodedKeySpec(keyAry);
			KeyFactory factory = KeyFactory.getInstance(KEY_ALGORITHM_RSA);
			Key key = keyType ? factory.generatePrivate(keySpec) : factory.generatePublic(keySpec);
			Cipher c = Cipher.getInstance(algorithm);
			c.init(Cipher.DECRYPT_MODE, key);
			return c.doFinal(dataAry);
		} catch (InvalidKeySpecException e) {
			LOG.error("RSA解密发生异常，无效的密钥规范", e);
		} catch (InvalidKeyException e) {
			LOG.error("RSA解密发生异常，无效的密钥", e);
		} catch (NoSuchAlgorithmException e) {
			LOG.error("RSA解密发生异常，没有[" + algorithm + "]算法", e);
		} catch (NoSuchPaddingException e) {
			LOG.error("RSA解密发生异常，无效的填充方法", e);
		} catch (IllegalBlockSizeException e) {
			LOG.error("RSA解密发生异常，非法的块长度", e);
		} catch (BadPaddingException e) {
			LOG.error("RSA解密发生异常，错误的填充方法", e);
		} 
		return null;
	}
	
	/**
	 * RSA加签-数组
	 * @param dataAry	待加签数据-数组
	 * @param priKeyAry	RSA加签私钥-数组
	 * @param algorithm	算法
	 * @return
	 */
	public static byte[] signByRSA(byte[] dataAry, byte[] priKeyAry, String algorithm) {
		try {
			PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(priKeyAry);		// 1.转换私钥材料
			KeyFactory factory = KeyFactory.getInstance(KEY_ALGORITHM_RSA);			// 2.实例化密钥工厂
			PrivateKey priKey = factory.generatePrivate(keySpec);					// 3.取私钥对象
			Signature st = Signature.getInstance(algorithm);						// 4.实例化Signature
			st.initSign(priKey);													// 5.初始化Signature
			st.update(dataAry);														// 6.更新
			return st.sign();														// 7.签名
		} catch (InvalidKeySpecException e) {
			LOG.error("RSA加签发生异常，无效的密钥规范", e);
		} catch (InvalidKeyException e) {
			LOG.error("RSA加签发生异常，无效的密钥", e);
		} catch (NoSuchAlgorithmException e) {
			LOG.error("RSA加签发生异常，没有[" + algorithm + "]算法", e);
		} catch (SignatureException e) {
			LOG.error("RSA加签发生异常，签名异常", e);
		} 
		return null;
	}

	/**
	 * RSA验签-数组
	 * @param dataAry	待验签数据-数组
	 * @param pubKeyAry	第三方公钥-数组
	 * @param signAry	签名-数组
	 * @param algorithm	算法
	 * @return
	 */
	public static boolean verifyByRSA(byte[] dataAry, byte[] pubKeyAry, byte[] signAry, String algorithm) {
		try {
			X509EncodedKeySpec keySpec = new X509EncodedKeySpec(pubKeyAry);				// 1.转换公钥
			KeyFactory factory = KeyFactory.getInstance(KEY_ALGORITHM_RSA);				// 2.实例化密钥工厂
			PublicKey pubKey = factory.generatePublic(keySpec);							// 3.生成公钥
			Signature st = Signature.getInstance(algorithm);							// 4.实例化Signature
			st.initVerify(pubKey);														// 5.初始化Signature
			st.update(dataAry);															// 6.更新
			return st.verify(signAry);													// 7.验签
		} catch (InvalidKeySpecException e) {
			LOG.error("RSA解签发生异常，无效的密钥规范", e);
		} catch (InvalidKeyException e) {
			LOG.error("RSA解签发生异常，无效的密钥", e);
		} catch (NoSuchAlgorithmException e) {
			LOG.error("RSA解签发生异常，没有[" + algorithm + "]算法", e);
		} catch (SignatureException e) {
			LOG.error("RSA解签发生异常，签名异常", e);
		} 
		return false;
	}
}
