/*
 * @(#)ACPConstants.java        1.0 2015年8月22日
 *
 * Copyright (c) 2007-2014 Shanghai Handpay IT, Co., Ltd.
 * No. 80 Xinchang Rd, Huangpu District, Shanghai, China
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of 
 * Shanghai Handpay IT Co., Ltd. ("Confidential Information").  
 * You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you 
 * entered into with Handpay.
 */
package com.hpay.core.common;

/**
 * @Description Class description goes here
 * @version 1.0
 * @author yhe
 * @since 2015年8月22日
 * @history	
 * 时间	版本	姓名	修改内容
 */
public class ACPConstants {
	
	/**==============================================================*/
	/** 基础常量												     	 */
	/**==============================================================*/
	/**
	 * <p>Discription:[ & 号]</p>
	 */
	public static final String AMPERSAND = "&";
	
	/**
	 * <p>Discription:[ { 号]</p>
	 */
	public static final String LEFT_BRACE = "{";
	
	/**
	 * <p>Discription:[ } 号]</p>
	 */
	public static final String RIGHT_BRACE = "}";
	
	/**
	 * 启用单证书模式(true:单证书|false:多证书---没有配置此项时,默认为单证书模式)
	 */
	public static final String SINGLE_MODE = "true";
	
	/**
	 * <p>Discription:[ = 号]</p>
	 */
	public static final String EQUAL = "=";
	
	/**
	 * <p>Discription:[ | 号]</p>
	 */
	public static final String COLON = "|";
	
	/**
	 * <p>Discription:[【银联返回】版本号]</p>
	 */
	public static final String UTF_8_ENCODING = "UTF-8";
	
	/**
	 * <p>Discription:[【银联返回】编码方式]</p>
	 */
	public static final String VERSION = "5.0.0";
	
	/**
	 * <p>Discription:[默认交易币种]</p>
	 */
	public static final String CURRENCY_CODE = "156";
	
	/**
	 * <p>Discription:[交易状态(与银联全渠道接口文档一致)：成功]</p>
	 */
	public static final String TRANSTYPE_STATUS_SUCCESS = "00";
	
	/**
	 * <p>Discription:[签名方法，取值：01（表示采用RSA）]</p>
	 */
	public static final String SIGN_METHOD = "01";
	
	/**
	 * <p>Discription:[机构有卡接入使用，标识支付要素使用的加密方式01:RSA密钥体系]</p>
	 */
	public static final String SECURITY_TYPE = "01";
	
	
	
	/**==============================================================*/
	/** 交易类型常量												     */
	/**==============================================================*/
	/**
	 * <p>Discription:[交易类型(与银联全渠道接口文档一致)：消费]</p>
	 */
	public static final String TRANSTYPE_DEBIT = "01";
	
	/**
	 * <p>Discription:[交易类型(与银联全渠道接口文档一致)：撤销]</p>
	 */
	public static final String TRANSTYPE_CANCEL = "31";
	
	/**
	 * <p>Discription:[交易类型(与银联全渠道接口文档一致)：退货]</p>
	 */
	public static final String TRANSTYPE_REFUND = "04";
	
	/**
	 * <p>Discription:[交易类型(与银联全渠道接口文档一致)：圈存]</p>
	 */
	public static final String TRANSTYPE_LOAD = "05";
	
	/**
	 * <p>Discription:[交易类型(与银联全渠道接口文档一致)：交易状态查询]</p>
	 */
	public static final String TRANSTYPE_STATUS_QUERY = "00";
	
	/**
	 * <p>Discription:[交易类型(与银联全渠道接口文档一致)：银行卡余额查询]</p>
	 */
	public static final String TRANSTYPE_BALANCE_QUERY = "71";
	
	/**
	 * <p>Discription:[交易类型(与银联全渠道接口文档一致)：代付]</p>
	 */
	public static final String TRANSTYPE_HELPPAY = "12";
	
	/**
	 * <p>Discription:[交易类型(与银联全渠道接口文档一致)：批量代付查询]</p>
	 */
	public static final String TRANSTYPE_BATCHQUERY = "22";
	
	/**
	 * <p>Discription:[交易类型(与银联全渠道接口文档一致)：批量代付交易]</p>
	 */
	public static final String TRANSTYPE_BATCHTRADE = "21";
	
	/**
	 * 交易子类型(与银联全渠道接口文档一致)：批量代付查询 01：退货 02：代收 03：代付
	 */
	public static final String BATCH_TXN_SUB_TYPE_03 = "03";
	
	/**
	 * <p>Discription:[交易类型(与银联全渠道接口文档一致)：对账文件下载]</p>
	 */
	public static final String TRANSTYPE_DOWNLOAD_FILE = "76";
	
	
	/**==============================================================*/
	/** 交易子类型常量												     */
	/**==============================================================*/
	/**
	 * <p>Discription:[交易子类型(与银联全渠道接口文档一致)：消费]</p>
	 */
	public static final String TXN_SUB_TYPE_01 = "01";
	
	/**
	 * <p>Discription:[交易子类型(与银联全渠道接口文档一致)：交易状态查询、消费撤销、消费退货、银行卡余额查询]</p>
	 */
	public static final String TXN_SUB_TYPE_00 = "00";
	
	/**
	 * <p>Discription:[交易子类型(与银联全渠道接口文档一致)：电子现金指定账户圈存]</p>
	 */
	public static final String TXN_SUB_TYPE_02 = "02";
	
	
	
	/**==============================================================*/
	/** 卡类型常量												     	 */
	/**==============================================================*/
	/**
	 * <p>Discription:[卡类型：IC卡]</p>
	 */
	public static final String CARD_TYPE_IC = "03";
	
	
	
	/**==============================================================*/
	/** 产品类型常量												     */
	/**==============================================================*/
	/**
	 * <p>Discription:[消费交易、交易状态查询、银行卡余额查询、电子现金指定账户圈存产品类型]</p>
	 */
	public static final String DEBIT_BIZ_TYPE = "000000";
	
	/**
	 * <p>Discription:[撤销、退货交易产品类型]</p>
	 */
	public static final String CANCEL_BIZ_TYPE = "000801";
	
	/**
	 * <p>Discription:[代付交易产品类型]</p>
	 */
	public static final String HELPPAY_BIZ_TYPE ="000401";
	
	/**
	 * <p>Discription:[对账文件下载产品类型]</p>
	 */
	public static final String DOWNLOAD_FILE_BIZ_TYPE ="000000";
	
	/**==============================================================*/
	/** 接入类型常量												     */
	/**==============================================================*/
	/**
	 * <p>Discription:[接入类型：0：普通商户直连接入；1：收单机构接入]</p>
	 */
	public static final String ACCESS_TYPE_0 = "0";
	
	/**
	 * <p>Discription:[接入类型：0：普通商户直连接入；1：收单机构接入]</p>
	 */
	public static final String ACCESS_TYPE_1 = "1";
	
	
	/**==============================================================*/
	/** 渠道类型常量												     */
	/**==============================================================*/
	/**
	 * <p>Discription:[渠道类型，取值05：语音，07：互联网，08：移动]</p>
	 */
	public static final String CHANNEL_TYPE_08 = "08";
	
	/**==============================================================*/
	/** 对账文件类型常量											     */
	/**==============================================================*/
	/**
	 * <p>Discription:[文件类型，00 表示【商户号（15位）_日期(YYYYMMDD).zip】]</p>
	 */
	public static final String FILE_TYPE_00 = "00";
	
	/**
	 * <p>Discription:[银联全渠道成功]</p>
	 */
	public static final String UNION_ACP_SUCCESS = "00,A6";
	
	/**
	 * <p>Discription:[银联全渠道批量代付返回处理中的状态]</p>
	 */
	public static final String UNION_ACP_BATCHDF_DONE = "03,04,05,01,12,34,60";
	
	/** 消费交易保留域 */
	public static final String TRANS_STATUS_QUERY_RESERVED = "交易状态查询";
	
	/** 代付交易保留域 */
	public static final String HELPPAY_RESERVED = "代付交易";
	
	/** 对账文件下载保留域 */
	public static final String DOWNLOAD_FILE_RESERVED = "对账文件下载";
	
	/** 未知 */
	public static final String UNKNOWN = "未知";
	
}
