package com.hpay.trans.gateway.exception;

import com.hpay.core.common.BaseException;

/**
 * �ӿ��쳣
 * @author jhjiang
 *
 */
public class HpayChannelException extends BaseException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3060767984598597644L;

	public HpayChannelException() {
	}

	public HpayChannelException(String errorCode, String errorMsg,
			Throwable caused) {
		super(errorCode, errorMsg, caused);
	}

	public HpayChannelException(String errorCode, String errorMsg) {
		super(errorCode, errorMsg);
	}

	public HpayChannelException(String errorCode, Throwable caused) {
		super(errorCode, caused);
	}

	public HpayChannelException(String errorCode) {
		super(errorCode);
	}
	
	

}
