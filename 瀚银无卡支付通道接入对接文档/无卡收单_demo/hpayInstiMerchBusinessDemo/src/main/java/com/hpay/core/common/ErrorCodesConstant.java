/*
 * @(#)ErrorCodesConstant.java        1.0 2015年8月20日
 *
 * Copyright (c) 2007-2014 Shanghai Handpay IT, Co., Ltd.
 * No. 80 Xinchang Rd, Huangpu District, Shanghai, China
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of 
 * Shanghai Handpay IT Co., Ltd. ("Confidential Information").  
 * You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you 
 * entered into with Handpay.
 */
package com.hpay.core.common;

/**
 * @Description 异常错误代码常量类 BaseException的errorCode采用该class的常量定义
 * @version 1.0
 * @author yhe
 * @since 2015年8月20日
 * @history 时间 版本 姓名 修改内容
 */
public class ErrorCodesConstant {

	/** 系统错误 */
	public static final String SYS_ERROR = "9999";

	/** 用户权限产生的异常 */
	public static final String E_AUTH = "AUTH";
	
	/** 资金方管理产生的异常 */
	public static final String E_CAPITAL = "CAPITAL";

	/** 由中间账户系统产生的异常 */
	public static final String E_SVS = "SVS";

	/** 由支付平台产生的异常 */
	public static final String E_PAYMENT = "PAYMENT";

	/** 由对账产生的异常 */
	public static final String E_RNCNL = "RNCNL";

	/** 由结算产生的异常 */
	public static final String E_SETTLE = "SETTLE";

	/** 商户信息管理产生的异常 */
	public static final String E_MERC = "MERC";

	/** 机构信息管理产生的异常 */
	public static final String E_INSTI = "INSTI";

	/** 参数信息管理产生的异常 */
	public static final String E_PARAMS = "PARAMS";
	
	/** 由数据访问层产生的异常 */
	public static final String E_DB = "D";

	/** 数据添加异常 */
	public static final String E_DB_SAVE = E_DB + "001";

	/** 数据更新异常 */
	public static final String E_DB_UPDATE = E_DB + "002";

	/** 数据删除异常 */
	public static final String E_DB_DEL = E_DB + "003";

	/** 数据查询异常 */
	public static final String E_DB_FIND = E_DB + "004";

	/** 查询中间账户信息异常 */
	public static final String E_SVS_ERROR = E_SVS + "001";

	/** 中间账户信息不存在 */
	public static final String E_SVS_03 = E_SVS + "003";

	/** 由连接器产生的异常 */
	public static final String E_CONNECTOR = "CONNECTOR";

	/** 不支持的接口类型 */
	public static final String E_CONNECTOR_NOT_SUPPORT_INTERFACE = E_CONNECTOR + "001";

	/** connector接口缺少必要参数 */
	public static final String E_CONNECTOR_LACK_OF_PARAM = E_CONNECTOR + "002";

	/** 生成请求报文失败 */
	public static final String E_CONNECTOR_CREATE_REQ_ERROR = E_CONNECTOR + "003";

	/** 商户连接失败 */
	public static final String E_CONNECTOR_MERCHANT_CONNECT_FAIL = E_CONNECTOR + "004";

	/** 获得商户应答报文失败 */
	public static final String E_CONNECTOR_GET_RESPONSE_FAIL = E_CONNECTOR + "005";

	/** 收到错误的应答报文 */
	public static final String E_CONNECTOR_RESPONSE_ERROR = E_CONNECTOR + "006";

	/** 商户答复操作代码与请求操作代码不符 */
	public static final String E_CONNECTOR_RESPCODE_NOT_TRANSCODE = E_CONNECTOR + "007";

	/** 报文加密错误 */
	public static final String E_CONNECTOR_ENCODE = E_CONNECTOR + "008";

	/** 密文解密错误 */
	public static final String E_CONNECTOR_DECODE = E_CONNECTOR + "009";

	/** 签名数据错误 */
	public static final String E_CONNECTOR_INVALID_SIGN = E_CONNECTOR + "010";
	
	/** 验证签名错误 */
	public static final String E_CONNECTOR_VELIDATE_INVALID_SIGN = E_CONNECTOR + "011";
	
	/** 压缩代付文件错误 */
	public static final String E_CONNECTOR_ENCODE_BATCH_DFFILE = E_CONNECTOR + "012";
	
	/** 解析代付文件错误 */
	public static final String E_CONNECTOR_ENCODE_PARSE_DFFILE = E_CONNECTOR + "013";
	
	/** HTTP通讯异常 */
	public static final String E_CONNECTOR_HTTP_ERROR = E_CONNECTOR + "020";
	
	/** HTTP请求异常 */
	public static final String E_CONNECTOR_HTTP_REQUEST_ERROR = E_CONNECTOR + "021";
	
	/** HTTP响应异常 */
	public static final String E_CONNECTOR_HTTP_RESPONSE_ERROR = E_CONNECTOR + "022";
	
	/** 无效的支付渠道 */
	public static final String E_PAYMENT_INVALID_BACKEND = E_PAYMENT + "001";

	/** 无效的对账渠道 */
	public static final String E_RNCNL_INVALID_BACKEND = E_RNCNL + "001";
	
	/** 对账异常 */
	public static final String E_RNCNL_ERROR = E_RNCNL + "002";
	
	/** 结算异常 */
	public static final String E_SETTLE_ERROR = E_SETTLE + "001";
	
	/** 没有找到对账文件 */
	public static final String E_RNCNL_NOT_FOUND = E_RNCNL + "001";
	
	/** 下载对账文件失败 */
	public static final String E_RNCNL_DOWN_ERROR = E_RNCNL + "002";
	
	/** 解压对账文件失败 */
	public static final String E_RNCNL_UNGZ_ERROR = E_RNCNL + "003";
	
	/** http异常 */

	/** 用户权限产生的异常 */
	public static final String E_AUTH_USER_EXIST = E_AUTH + "01";
	public static final String R_NE_AUTH_USER_NOT_EXIST = E_AUTH + "02";
	public static final String E_AUTH_USER_INS_MGR_EXIST = E_AUTH + "03";
	public static final String R_NE_AUTH_USER_INIT = E_AUTH + "04";
	public static final String E_AUTH_ROLE_EXIST = E_AUTH + "05";
	public static final String E_AUTH_ROLE_NO_PERMISSION = E_AUTH + "06";
	public static final String E_AUTH_USER_INS_MGR_EXIST_MSG = "机构管理员已经存在！";
	public static final String E_AUTH_USER_CAP_MGR_EXIST_MSG = "该出资方用户已经存在！";
	public static final String E_AUTH_USER_MER_MGR_EXIST_MSG = "商户管理员已经存在！";
	public static final String E_AUTH_USER_EXIST_MSG = "用户名已经存在！";
	public static final String E_AUTH_ROLE_EXIST_MSG = "角色名已经存在！";
	public static final String E_AUTH_ROLE_NO_PERMISSION_MSG = "无权限做该操作！";
	public static final String E_AUTH_USER_NOT_EXIST_MSG = "用户不存在！";
	public static final String R_NE_AUTH_USER_INIT_MSG = "用户状态初始化不能冻结！";
	
	/** 资金方管理异常 */
	public static final String E_CAPITAL_EXIST = E_CAPITAL + "01";
	public static final String E_CAPITAL_EXIST_MSG =  "资金方名称已经存在！";
	public static final String E_CAPITAL_NOT_EXIST = E_CAPITAL + "02";
	public static final String E_CAPITAL_NOT_EXIST_MSG =  "资金方信息不存在！";
}
