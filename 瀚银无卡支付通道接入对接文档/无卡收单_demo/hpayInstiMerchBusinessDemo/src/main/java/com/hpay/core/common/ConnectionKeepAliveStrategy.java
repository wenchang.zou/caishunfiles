package com.hpay.core.common;

import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.HttpResponse;
import org.apache.http.impl.client.DefaultConnectionKeepAliveStrategy;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.log4j.Logger;

public class ConnectionKeepAliveStrategy extends DefaultConnectionKeepAliveStrategy {

	/**
	 * <p>Discription:[日志]</p>
	 */
	private static final Logger logger = Logger.getLogger(ConnectionKeepAliveStrategy.class);
	
	@Override
	public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
		
		/*long keepAliveDuration = super.getKeepAliveDuration(response, context);
		if (keepAliveDuration == -1) {
			logger.info("ConnectionKeepAliveStrategy#getKeepAliveDuration keepAliveDuration="+keepAliveDuration);
			keepAliveDuration = 100;
		}
		logger.info("getKeepAliveDuration keepAliveDuration="+keepAliveDuration);
		return keepAliveDuration;*/
		
		long keepAliveDuration = 100;
		final HeaderElementIterator it = new BasicHeaderElementIterator(response.headerIterator(HTTP.CONN_KEEP_ALIVE));
		while (it.hasNext()) {
			final HeaderElement he = it.nextElement();
			final String param = he.getName();
			final String value = he.getValue();
			
			if (value != null && param.equalsIgnoreCase("timeout")) {
				try {
					keepAliveDuration = Long.parseLong(value) * 1000;
					logger.info("http长连接保持时间【keepAliveDuration】=" + keepAliveDuration);
				} catch (final NumberFormatException ignore) {
					logger.error(ignore);
				}
			}
			if (value != null && param.equalsIgnoreCase("max")) {
				Long max = Long.parseLong(value);
				if (max <= 0) {
					keepAliveDuration = 100;
					logger.info("http服务端最大长连接数【max】=" + keepAliveDuration);
				}
			}
		}
		logger.info("getKeepAliveDuration#keepAliveDuration=" + keepAliveDuration);
		return keepAliveDuration;
	}
}
