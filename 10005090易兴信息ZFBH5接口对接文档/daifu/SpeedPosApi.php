<?php

/**
 * Class SpeedPosApi
 * SpeedPos收单系统API接口
 */
class SpeedPosApi
{
    private $mch_id; // 商户ID
    private $mch_key; // 商户KEY
    private $parameters;
    private $baseUrl = 'https://rpi.szyinfubao.com'; // 接口地址
	private $sign_type;
    private $debug = false;

    public function __construct($mch_id = '', $mch_key = '')
    {
        $this->mch_id = $mch_id;
        $this->mch_key = $mch_key;
		$this->sign_type = 'MD5';
        $this->parameters = [];
		$this->setParameter('mch_id', $mch_id);
    }

    /**
     * 单独设置mch_id
     *
     * @param $mch_id
     */
    public function setMchId($mch_id)
    {
        $this->mch_id = $mch_id;
    }

    /**
     * 单独设置mch_key
     *
     * @param $mch_key
     */
    public function setMchKey($mch_key)
    {
        $this->mch_key = $mch_key;
    }

    public function setBaseUrl($url)
    {
        $this->baseUrl = $url;
    }

    /**
     * 签名校验
     *
     * @param string $xml
     *
     * @return bool
     */
    public function signVerify($data)
    {
        $this->setParameters($data);
        $sign = $this->makeSign();
        if ($data['signature'] === $sign) {
            return true;
        }

        return false;
    }

    /**
     * 开启调试
     */
    public function debug()
    {
        $this->debug = true;
    }

    /**
     * 设置参数
     *
     * @param string $key
     * @param string $value
     */
    public function setParameter($key = '', $value = '')
    {
        $this->parameters[$key] = $value;
    }

    /**
     * 获取参数值
     *
     * @param $key
     *
     * @return string
     */
    public function getParameter($key)
    {
        return isset($this->parameters[$key]) ? $this->parameters[$key] : '';
    }

    /**
     * 批量设置参数
     *
     * @param array $arr
     */
    public function setParameters($arr = [])
    {
        foreach ($arr as $key => $value) {
            if ($value) {
                $this->setParameter($key, $value);
            }
        }
    }

    /**
     * 生成签名
     *
     * @return string
     */
    public function makeSign()
    {
        unset($this->parameters['_url']);
        //签名步骤一：按字典序排序参数
        ksort($this->parameters);
        $this->biz_content = json_encode($this->parameters, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES );
        //签名步骤二：在string后加入KEY
        $string = 'biz_content=' . $this->biz_content . "&key=" . $this->mch_key;
        //签名步骤三：MD5加密
        //echo $string;echo "\r\n";
        $string = md5($string);
        //签名步骤四：所有字符转为大写
        $result = strtoupper($string);

        return $result;
    }

    /**
     * 清空所有参数
     */
    public function clear()
    {
        $this->parameters = [];
    }

    /**
     * 请求接口
     *
     * @param string $url
     * @param array  $data
     * @param string $method
     *
     * @return mixed
     */
    public function requestApi($url = '', $data = [], $method = 'p')
    {
        $this->setParameters($data,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        $sign = $this->makeSign();
		$this->clear();
		$this->setParameter('biz_content', $this->biz_content);
        $this->setParameter('signature', $sign);
		$this->setParameter('sign_type', $this->sign_type);

        $_method = $method == 'g' ? 'GET' : 'POST';
        $result = $this->doRequest($url, $this->parameters, $_method);
       
        if ($this->debug) {
            echo '<pre>';
            echo 'Url：' . $url;
            echo '<br><br>Parameters：<br>';
            print_r($this->parameters);
            echo '<br>Result：<br>';
            print_r($result);
            echo '</pre>';
        }

        $this->clear();

        return $result;
    }

    /** http请求
     *
     * @param        $url
     * @param array  $params
     * @param string $method
     *
     * @return mixed
     */
    public function doRequest($url, $params = [], $method = 'GET')
    {
        if (!function_exists('curl_init')) {
            exit('Need to open the curl extension');
        }
        if ($method == 'GET') {
            $url .= '?' . http_build_query($params);
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_HEADER, 0); //展示响应头
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);//设置连接等待时间,0不等待
        if ($method == 'POST') {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        }

        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }
}