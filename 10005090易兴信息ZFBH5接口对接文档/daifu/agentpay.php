<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1" /> 
    <title>Speedpos支付样例-快捷签约绑卡</title>
</head>
<body>
    <?php
        if ($_POST) {
            require_once 'SpeedPosApi.php';
            $speedPosApi = new SpeedPosApi($_POST['mch_id'], $_POST['mch_key']);
            $url = $_POST['baseUrl'] . 'agentpay/pay';
            unset($_POST['baseUrl']);
            $speedPosApi->debug();
            $result = $speedPosApi->requestApi($url, $_POST);
        }
    ?>
    <form action="#" method="post">
    <div style="margin-left:2%;">选择环境：</div>
        <br/>
        <select name="baseUrl" style="width:96%;height:35px;margin-left:2%;">
            <option value="http://rpidev.speedpos.in/">开发环境</option>
            <option value="http://rpi.snsshop.net/">测试环境</option>
            <option value="https://rpi.speedpos.cn/">正式环境</option>
        </select>
        <br /><br />
        <div style="margin-left:2%;">商户号：</div><br/>
        <input type="text" style="width:96%;height:35px;margin-left:2%;" name="mch_id" /><br /><br />
        <div style="margin-left:2%;">商户key：</div> <br/>
        <input type="text" style="width:96%;height:35px;margin-left:2%;" name="mch_key" /> <br /><br />
        <div style="margin-left:2%;">商户订单号：</div><br/>
        <input type="text" style="width:96%;height:35px;margin-left:2%;" name="out_order_no" value="<?= time().mt_rand(1000, 9999) ?>" /><br /><br />
        <div style="margin-left:2%;">交易金额(单位：分)：</div><br/>
        <input type="text" style="width:96%;height:35px;margin-left:2%;" name="payment_fee" /><br /><br />
        <div id="other_fields">
            <div style="margin-left:2%;">收款人账号：</div><br/>
            <input type="text" style="width:96%;height:35px;margin-left:2%;" name="payee_acct_no" /><br /><br />
            <div style="margin-left:2%;">收款人名字：</div><br/>
            <input type="text" style="width:96%;height:35px;margin-left:2%;" name="payee_acct_name" /><br /><br />
            <div style="margin-left:2%;">银行卡类型：</div><br/>
            <select name="card_type" style="width:96%;height:35px;margin-left:2%;">
                <option value="1">借记卡</option>
                <option value="2">贷记卡</option>
            </select><br /><br />
            <div style="margin-left:2%;">cvv2：</div><br/>
            <input type="text" style="width:96%;height:35px;margin-left:2%;" name="cvv2" placeholder="卡类型为贷记卡填写" /><br /><br />
            <div style="margin-left:2%;">支行联行号：</div><br/>
            <input type="text" style="width:96%;height:35px;margin-left:2%;" name="payee_branch_no" placeholder="必传" /><br /><br />
            <div style="margin-left:2%;">支行名称：</div><br/>
            <input type="text" style="width:96%;height:35px;margin-left:2%;" name="payee_branch_name" placeholder="对公必传" /><br /><br />
            <div style="margin-left:2%;">备注：</div><br/>
            <input type="text" style="width:96%;height:35px;margin-left:2%;" name="remark" /><br /><br />
            <div style="margin-left:2%;">收款人手机号：</div><br/>
            <input type="text" style="width:96%;height:35px;margin-left:2%;" name="payee_phone_num" /><br /><br />
            <div style="margin-left:2%;">收款人账号类型：</div><br/>
            <select name="payee_acct_type" style="width:96%;height:35px;margin-left:2%;">
                <option value="1">对私</option>
                <option value="2">对公</option>
            </select>
            <br /><br />
            <div style="margin-left:2%;">证件号：</div><br/>
            <input type="text" style="width:96%;height:35px;margin-left:2%;" name="id_num" /><br /><br />
			 <div style="margin-left:2%;">结算类型:</div><br/>
            <input type="text" style="width:96%;height:35px;margin-left:2%;" name="settle_type" /><br /><br />
            <div style="margin-left:2%;">异步通知地址：</div><br/>
            <input type="text" style="width:96%;height:35px;margin-left:2%;" name="notify_url" value=""/><br /><br />
        </div>
        <div align="center">
            <input type="submit" value="提交" style="width:210px; height:50px; border-radius: 15px;background-color:#FE6714; border:0px #FE6714 solid; cursor: pointer;  color:white;  font-size:16px;" />
        </div>
    </form>

    <script type="text/javascript" src="static/zepto.min.js"></script>
    <script type="text/javascript">
        $(function() {
            $('#order_type').on('change', function() {
                var value = $(this).val();
                if (value == 2) {
                    $('#other_fields').hide();
                } else {
                    $('#other_fields').show();
                }
            });
        });
    </script>
</body>
</html> 