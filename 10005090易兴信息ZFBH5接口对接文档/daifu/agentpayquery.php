<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1" /> 
    <title>Speedpos支付样例-代付订单查询</title>
</head>
<body>
    <?php
        if ($_POST) {
            require_once 'SpeedPosApi.php';
            $speedPosApi = new SpeedPosApi($_POST['mch_id'], $_POST['mch_key']);
            $url = $_POST['baseUrl'] . 'agentpay/query';
            unset($_POST['baseUrl']);
            $speedPosApi->debug();
            $result = $speedPosApi->requestApi($url, $_POST);
        }
    ?>
    <form action="#" method="post">
    <div style="margin-left:2%;">选择环境：</div>
        <br/>
        <select name="baseUrl" style="width:96%;height:35px;margin-left:2%;">
            <option value="http://rpidev.speedpos.in/">开发环境</option>
            <option value="http://rpi.snsshop.net/">测试环境</option>
            <option value="https://rpi.speedpos.cn/">正式环境</option>
        </select>
        <br />
        <br />
        <div style="margin-left:2%;">订单号：</div><br/>
        <input type="text" style="width:96%;height:35px;margin-left:2%;" name="order_no" /><br /><br />
        <div style="margin-left:2%;">商户订单号：</div><br/>
        <input type="text" style="width:96%;height:35px;margin-left:2%;" name="out_order_no" /><br /><br />
        <div style="margin-left:2%;">商户号：</div><br/>
        <input type="text" style="width:96%;height:35px;margin-left:2%;" name="mch_id" /><br /><br />
        <div style="margin-left:2%;">商户key：</div> <br/>
        <input type="text" style="width:96%;height:35px;margin-left:2%;" name="mch_key" /> <br /><br />
        <div align="center">
            <input type="submit" value="查询" style="width:210px; height:50px; border-radius: 15px;background-color:#FE6714; border:0px #FE6714 solid; cursor: pointer;  color:white;  font-size:16px;" />
        </div>
    </form>
</body>
</html> 