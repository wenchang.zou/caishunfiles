<?php
require_once 'SpeedPosApi.php';
require_once 'config.php';


/**
* 流程：
* 1、调用统一下单，取得code_url，生成二维码
* 2、用户扫描二维码，进行支付
* 3、支付完成之后，微信服务器会通知支付成功
* 4、在支付成功通知中需要查单确认是否真正支付成功（见：notify.php）
*/
if (isset($_POST['mch_id']))
{
	$mch_id = $_POST['mch_id'];
	$mch_key = $_POST['mch_key'];
	$speedPosApi = new SpeedPosApi($mch_id, $mch_key);
    if (! $_POST['out_order_no']) {
        $_POST['out_order_no'] = $_POST['mch_id'] . date('YmdHis') . rand(10000, 99999);
    }
    $_POST['cur_type'] = 'CNY';
    $url = $_POST['baseUrl'] . 'unifiedorder';
    unset($_POST['baseUrl']);
    $speedPosApi->debug();
    $result = $speedPosApi->requestApi($url, $_POST);
    $res = json_decode($result, true);
}
?>

  <html>

  <head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>综合测试</title>
	<?php if (isset($res['ret_code']) && $res['ret_code'] === "0" && $_POST['pay_platform'] == 'SQPAY' && $_POST['pay_type'] == 'JSAPI') {?>
    <script src="https://open.mobile.qq.com/sdk/qqapi.js?_bid=152"></script>
    <?php } ?>
  </head>

  <body>
    <?php if (isset($res['ret_code']) && $res['ret_code'] === "0" && $_POST['pay_type'] == 'NATIVE') {?>
      <div style="margin-left: 10px;color:#556B2F;font-size:30px;font-weight: bolder;">扫描支付</div>
      <br/>
      <img alt="扫码支付" src="http://qr.liantu.com/api.php?text=<?php echo urlencode($res['biz_content']['qrcode']);?>" style="width:150px;height:150px;" />
      <br/>
      <br/>
      <br/>
      <?php } ?>

        <?php if (isset($res['ret_code']) && $res['ret_code'] === "0" && $_POST['pay_platform'] == 'WXPAY' && $_POST['pay_type'] == 'JSAPI') {?>
          <script type="text/javascript">
            //调用微信JS api 支付
            function jsApiCall() {
              WeixinJSBridge.invoke(
                'getBrandWCPayRequest',
                <?php echo json_encode($res['biz_content']['pay_params']); ?>,
                function(res) {
                  WeixinJSBridge.log(res.err_msg);
                  alert(res.err_code + res.err_desc + res.err_msg);
                }
              );
            }

            function callpay() {
              if (typeof WeixinJSBridge == "undefined") {
                if (document.addEventListener) {
                  document.addEventListener('WeixinJSBridgeReady', jsApiCall, false);
                } else if (document.attachEvent) {
                  document.attachEvent('WeixinJSBridgeReady', jsApiCall);
                  document.attachEvent('onWeixinJSBridgeReady', jsApiCall);
                }
              } else {
                jsApiCall();
              }
            }
            callpay()
          </script>
          <?php } ?>

            <?php if (isset($res['ret_code']) && $res['ret_code'] === "0" && $_POST['pay_platform'] == 'ALIPAY' && $_POST['pay_type'] == 'JSAPI') {?>
              <script type="text/javascript">
                document.addEventListener('AlipayJSBridgeReady', function() {
                  AlipayJSBridge.call("tradePay", <?php echo json_encode($res['biz_content']['pay_params']); ?>, function(res) {
                    alert(res.resultCode);
                  });
                }, false);
              </script>
              <?php } ?>
			  
              
              <?php if (isset($res['ret_code']) && $res['ret_code'] === "0" && $_POST['pay_platform'] == 'SQPAY' && $_POST['pay_type'] == 'JSAPI') {?>
                <script type="text/javascript">
                      mqq.tenpay.pay({
                			tokenId: "<?php echo $res['biz_content']['pay_params']['tokenId'] ?>"
                		}, function (result, resultCode) {
                    		if (resultCode === undefined) {
                    			resultCode = result.resultCode;
                    		}
                    		if (resultCode) {            			
                    			alert("支付错误:" + res.retmsg);
                    		}
                	});
                 </script>
              <?php } ?>
			  
                <form action="#" method="post">
                  <div style="margin-left:2%;">选择环境：</div>
                  <br/>
                  <select name="baseUrl" style="width:96%;height:35px;margin-left:2%;">
                    <option value="http://rpidev.speedpos.in/">开发环境</option>
                    <option value="http://rpi.snsshop.net/">测试环境</option>
                    <option value="https://rpi.speedpos.cn/">正式环境</option>
                  </select>
                  <br />
                  <br />
                  <div style="margin-left:2%;">商户号：</div>
                  <br/>
                  <input type="text" style="width:96%;height:35px;margin-left:2%;" name="mch_id" />
                  <br />
                  <br />
                  <div style="margin-left:2%;">商户key：</div>
                  <br/>
                  <input type="text" style="width:96%;height:35px;margin-left:2%;" name="mch_key" />
                  <br />
                  <br />
                  <div style="margin-left:2%;">支付平台：</div>
                  <br/>
                  <select style="width:96%;height:35px;margin-left:2%;" name="pay_platform">
                    <option value="WXPAY">微信</option>
                    <option value="ALIPAY">支付宝</option>
                    <option value="SQPAY">手Q</option>
                    <option value="BDPAY">百度</option>
                    <option value="JDPAY">京东</option>
                  </select>
                  <br />
                  <br />

                  <div style="margin-left:2%;">支付类型：</div>
                  <br/>
                  <select style="width:96%;height:35px;margin-left:2%;" name="pay_type">
                    <option value="JSAPI">公众号</option>
                    <option value="NATIVE">扫码支付</option>
                    <option value="APP">手机app支付</option>
                    <option value="MICROPAY">刷卡(条码支付)</option>
                    <option value="WAP">手机wap支付</option>
					<option value="MWEB">H5支付</option>
                  </select>
                  <br />
                  <br />

                  <div style="margin-left:2%;">商户订单号：</div>
                  <br/>
                  <input type="text" style="width:96%;height:35px;margin-left:2%;" name="out_order_no" placeholder="不填则自动生成" />
                  <br />
                  <br />

                  <div style="margin-left:2%;">交易金额，单位分：</div>
                  <br/>
                  <input type="text" style="width:96%;height:35px;margin-left:2%;" name="payment_fee" />
                  <br />
                  <br />

                  <div style="margin-left:2%;">商品信息：</div>
                  <br/>
                  <input type="text" style="width:96%;height:35px;margin-left:2%;" name="body" />
                  <br />
                  <br />

                  <div style="margin-left:2%;">商户发起请求时的IP：</div>
                  <br/>
                  <input type="text" style="width:96%;height:35px;margin-left:2%;" name="bill_create_ip" />
                  <br />
                  <br />

                  <div style="margin-left:2%;">门店编号：</div>
                  <br/>
                  <input type="text" style="width:96%;height:35px;margin-left:2%;" name="shop_id" />
                  <br />
                  <br />

                  <div style="margin-left:2%;">店员编号：</div>
                  <br/>
                  <input type="text" style="width:96%;height:35px;margin-left:2%;" name="cashier_id" />
                  <br />
                  <br />

                  <div style="margin-left:2%;">订单拓展信息：</div>
                  <br/>
                  <input type="text" style="width:96%;height:35px;margin-left:2%;" name="remark" />
                  <br />
                  <br />

                  <div style="margin-left:2%;">付款码：</div>
                  <br/>
                  <input type="text" style="width:96%;height:35px;margin-left:2%;" name="auth_code" placeholder="支付类型为刷卡时必填" />
                  <br />
                  <br />

                  <div style="margin-left:2%;">用户标识：</div>
                  <br/>
                  <input type="text" style="width:96%;height:35px;margin-left:2%;" name="openid" placeholder="支付类型为公众号时必填" />
                  <br />
                  <br />

                  <div style="margin-left:2%;">公众号或者扫码支付时，信用卡限制标识，如果禁用信用卡支付，只能传no_credit</div>
                  <br/>
                  <input type="text" style="width:96%;height:35px;margin-left:2%;" name="pay_limit" />
                  <br />
                  <br />

                  <div style="margin-left:2%;">通知地址：</div>
                  <br/>
                  <input type="text" style="width:96%;height:35px;margin-left:2%;" name="notify_url" />
                  <br />
                  <br />

                  <div align="center">
                    <input type="submit" value="确认" style="width:210px; height:50px; border-radius: 15px;background-color:#FE6714; border:0px #FE6714 solid; cursor: pointer;  color:white;  font-size:16px;" />
                  </div>
                </form>

  </body>

  </html>