<?php
// 服务器异步回调

require_once 'SpeedPosApi.php';
require_once 'config.php';
$speedPosApi = new SpeedPosApi($mch_id, $mch_key);
$verify = $speedPosApi->signVerify($_POST);

if ($verify) { // 签名验证通过
	// 这里商户可以做一些自己的验证方式，如对比订单金额等
}
echo $verify ? 'success' : 'fail';