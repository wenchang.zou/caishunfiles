<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/> 
    <title>Speedpos支付样例</title>
    <style type="text/css">
        ul {
            margin-left:10px;
            margin-right:10px;
            margin-top:10px;
            padding: 0;
        }
        li {
            width: 32%;
            float: left;
            margin: 0px;
            margin-left:1%;
            padding: 0px;
            height: 100px;
            display: inline;
            line-height: 100px;
            color: #fff;
            font-size: x-large;
            word-break:break-all;
            word-wrap : break-word;
            margin-bottom: 5px;
        }
        a {
            display: block;
            -webkit-tap-highlight-color: rgba(0,0,0,0);
        	text-decoration:none;
            color:#fff;
        }
        a:link{
            -webkit-tap-highlight-color: rgba(0,0,0,0);
        	text-decoration:none;
            color:#fff;
        }
        a:visited{
            -webkit-tap-highlight-color: rgba(0,0,0,0);
        	text-decoration:none;
            color:#fff;
        }
        a:hover{
            -webkit-tap-highlight-color: rgba(0,0,0,0);
        	text-decoration:none;
            color:#fff;
        }
        a:active{
            -webkit-tap-highlight-color: rgba(0,0,0,0);
        	text-decoration:none;
            color:#fff;
        }
        .main {
        	float: left; 
        	width: 100%;
        }
        </style>
</head>
<body>
	<div class="main" align="center">
	    <h1>微信/支付宝</h1>
        <ul>
            <li style="background-color:#FF7F24"><a href="all.php">综合测试</a></li>  
            <li style="background-color:#CDCD00"><a href="orderquery.php">订单查询</a></li>
            <li style="background-color:#CD3278"><a href="refund.php">订单退款</a></li>
            <li style="background-color:#848484"><a href="refundquery.php">退款查询</a></li>
            <li style="background-color:#CDCD00"><a href="closeorder.php">关闭订单</a></li>
            <li style="background-color:#8EE5EE"><a href="download.php">下载账单</a></li> 
        </ul>
	</div>
  
	<div class="main" align="center">
	    <h1>快捷</h1>
        <ul>       
            <li style="background-color:#a369af"><a href="quickpay.php">快捷支付下单-模式一</a></li>     
            <li style="background-color:#a369af"><a href="quicksubmitcode.php">快捷支付短信确认</a></li>
            <li style="background-color:#a369af"><a href="quickresend.php">快捷支付短信重发</a></li>
            <li style="background-color:#a369af"><a href="gotoQuickPay.php?type=2">快捷支付下单-模式二-已绑定卡</a></li>  
            <li style="background-color:#a369af"><a href="gotoQuickPay.php?type=1">快捷支付下单-模式二-未绑定卡</a></li>  
            <li style="background-color:#a369af"><a href="quickquery.php">快捷订单查询</a></li> 
            <li style="background-color:#a369af"><a href="quickbindcard.php">快捷签约绑卡</a></li>
            <li style="background-color:#a369af"><a href="quickunbind.php">快捷签约解绑</a></li>
            <li style="background-color:#a369af"><a href="quickquerybind.php">快捷签约查询</a></li>
        </ul>
    </div>
    
    
    <div class="main" align="center">
	    <h1>代付</h1>
        <ul> 
            <li style="background-color:#cdcd00"><a href="agentpay.php">单笔代付发起</a></li>
            <li style="background-color:#cdcd00"><a href="agentpayquery.php">单笔代付查询</a></li>
        </ul>
    </div>
    
    
    <div class="main" align="center">
	    <h1>网银</h1>
        <ul> 
            <li style="background-color:#cdcd00"><a href="ecurrencypay.php">网银下单</a></li>
            <li style="background-color:#cdcd00"><a href="ecurrencypayquery.php">网银订单查询</a></li>
        </ul>
    </div>
</body>
</html>