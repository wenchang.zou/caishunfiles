<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page session="false" %>
<html>
<head>
	<title>聚鑫测试支付</title>
	<script src="<%=request.getContextPath()%>/resources/jquery.min.js"></script>
	<script src="<%=request.getContextPath()%>/resources/bootstrap.js"></script>
	<script src="<%=request.getContextPath()%>/resources/bootstrap.min.js"></script>
	<script src="<%=request.getContextPath()%>/resources/bootstrap-select.js"></script>
	<!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
	<link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<!-- 可选的 Bootstrap 主题文件（一般不用引入） -->
	<link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
	<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	
</head>
<body>
	<div class="panel panel-info">
	  <div class="panel-heading">测试支付</div>
	  <div class="panel-body">
	    <form action="/testPay/testPay" method="get"  target="_blank" id="testPayForm">
			 <div style="height:40px;">
			 	<div class="form-group form-group-lg">
				    <div class="col-sm-2">
				     	 商户id：
				    </div>
				    <div class="col-sm-2">
				      <input class="" type="text" id="agentId" name="agentId">
				    </div>
				 </div>
				 <div class="form-group form-group-lg">
				 	<div class="col-sm-2">
				     	支付方式：
				    </div>
				    <div class="col-sm-2">
				      <select name="payType" id="payType" onclick="payTypex()">
						<option value="10"  selected>微信扫码支付
						<option value="20">微信wap支付
						<option value="30">支付宝扫码支付
						<option value="40">支付宝wap支付
						<option value="50">网关支付
						<option value="50UN">银联扫码
						<option value="50JD">京东支付
						<option value="60">QQ钱包扫码支付
						<option value="70">QQ钱包wap支付
					  </select>
				    </div>
				 </div>
				 <div class="form-group form-group-lg" id="selectBank" style="display: none;">
				    <div class="col-sm-2">
				     	选择银行：
				    </div>
				    <div class="col-sm-2">
				      <select name="bankCode" id="bankCode">
						<option value="ICBC"  selected>工商银行
						<option value="CCB">建设银行
						<option value="ABC">农业银行
						<option value="CMB">招商银行
						<option value="COMM">交通银行
						<option value="CMBC">民生银行
						<option value="CIB">兴业银行
						<option value="CEB">光大银行
						<option value="CITIC">中信银行
						<option value="GZCB">广州银行
						<option value="HXB">华夏银行
						<option value="BOC">中国银行
						<option value="BJRCB">北京农商行
						<option value="BCCB">北京银行
						<option value="SDB">深圳发展银行
						<option value="SZPAB">平安银行
						<option value="BOS">上海银行
						<option value="PSBC">中国邮政储蓄银行
						<option value="SPDB">浦东发展银行
						<option value="GDB">广东发展银行
						<option value="UNIONPAY">银联扫码
						<option value="JDPAY">京东扫码
					 </select>
				    </div>
				 </div>
			 </div>
			 <div style="height:40px;">
			 	<div class="form-group form-group-lg">
				    <div class="col-sm-2">
				     	 订单金额：
				    </div>
				    <div class="col-sm-2">
				      <input id="payAmt" name="payAmt" 
				      	onkeyup="if(isNaN(value))execCommand('undo');this.value = this.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');"
				      		onafterpaste="if(isNaN(value))execCommand('undo')">
				    </div>
				 </div>
			 	<div class="form-group form-group-lg">
				    <div class="col-sm-2">
				     	 商户Key：
				    </div>
				    <div class="col-sm-2">
				      <input id="agentKey" name="agentKey">
				    </div>
				 </div>
				 <button type="button" class="btn btn-default" style="float: right;margin-right: 30px;" onclick="testPay()">支付测试</button>
			 </div>
		</form>
	  </div>
	</div>
	<div class="panel panel-warning">
	  <div class="panel-heading">测试代付</div>
	  <div class="panel-body">
	    <form id="queryForm" name="queryForm" action="/testPay/testPayForAnother">
	    	<input type="hidden" id="agentOrderId" name="agentOrderId" value="${data.agentOrderId}">
			<input type="hidden" id="version" name="version" value="${data.version}">
	    	<div style="height:40px;">
		    	<div class="form-group form-group-lg">
				    <div class="col-sm-2">
				     	 商户id：
				    </div>
				    <div class="col-sm-2">
				      <input class="" type="text" id="agentId2" name="agentId2" value="${data.agentId}">
				    </div>
				 </div>
				 <div class="form-group form-group-lg">
				 	<div class="col-sm-2">
				     	付款银行：
				    </div>
				    <div class="col-sm-2">
				      <select name="bankCode2" id="bankCode2" value="${data.bankCode}">
						<option value="1">工商银行
						<option value="2">建设银行
						<option value="3">农业银行
						<option value="4">邮政储蓄银行
						<option value="5">中国银行
						<option value="6">交通银行
						<option value="7">招商银行
						<option value="8">光大银行
						<option value="9">浦发银行
						<option value="10">华夏银行
						<option value="11">广东发展银行
						<option value="12">中信银行
						<option value="13">兴业银行
						<option value="14">民生银行
						<option value="15">杭州银行
						<option value="16">上海银行
						<option value="17">宁波银行
						<option value="18">平安银行
					  </select>
				    </div>
				 </div>
				 <div class="form-group form-group-lg">
				    <div class="col-sm-2">
				     	对公对私：
				    </div>
				    <div class="col-sm-2">
				        <select name="payeeType" id="payeeType" value="${data.payeeType}">
							<option value="0">对公账户
							<option value="1">对私账户
						</select>
				    </div>
				 </div>
			 </div>
			 <div style="height:40px;">
				 <div class="form-group form-group-lg">
				    <div class="col-sm-2">
				     	 收款人姓名：
				    </div>
				    <div class="col-sm-2">
				      <input id="payeeName" name="payeeName" value="${data.payeeName}">
				    </div>
				 </div>
				 <div class="form-group form-group-lg">
				    <div class="col-sm-2">
				     	 收款人账号：
				    </div>
				    <div class="col-sm-2">
				    	<input id="payeeAccount" name="payeeAccount" value="${data.payeeAccount}" 
				    		onkeyup="this.value=this.value.replace(/\D/g,'')" 
				    			onafterpaste="this.value=this.value.replace(/\D/g,'')" maxlength="19">
				    </div>
				 </div>
				 <div class="form-group form-group-lg">
				    <div class="col-sm-2">
				     	收款支行名称：
				    </div>
				    <div class="col-sm-2">
				      <input id="payeeOpeningBank" name="payeeOpeningBank" value="${data.payeeOpeningBank}">
				    </div>
				 </div>
			 </div>
			 <div style="height:40px;">
				 <div class="form-group form-group-lg">
				    <div class="col-sm-2">
				     	省份：
				    </div>
				    <div class="col-sm-2">
				      <input id="province" name="province" value="${data.province}">
				    </div>
				 </div>
				 <div class="form-group form-group-lg">
				    <div class="col-sm-2">
				     	城市：
				    </div>
				    <div class="col-sm-2">
				      <input id="city" name="city" value="${data.city}">
				    </div>
				 </div>
				 <div class="form-group form-group-lg">
				    <div class="col-sm-2">
				     	订单金额：
				    </div>
				    <div class="col-sm-2">
				      <input id="amount" name="amount" value="${data.amount}" 
				      	onkeyup="if(isNaN(value))execCommand('undo');this.value = this.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');"
				      		onafterpaste="if(isNaN(value))execCommand('undo')">
				    </div>
				 </div>
			 </div>
			 <div style="height:40px;">
			 	<div class="col-sm-2">
			     	商户key：
			    </div>
			    <div class="col-sm-2">
			      <input id="agentKey2" name="agentKey2" value="${data.agentKey}">
			    </div>
			 </div>
			 <div style="height:60px;">
			 	<div class="col-sm-2">
			     	备注：
			    </div>
			    <div class="col-sm-2">
			      <textarea id="remark" name="remark" class="form-control" rows="3">${data.remark}</textarea>
			    </div>
			 </div>
			 <div style="height:40px;">
			 	<button type="button" class="btn btn-default" style="float: right;margin-right: 30px;" onclick="testPayForAnother()">代付测试</button>
			 </div>
		</form>
		<div style="height:40px;">
			<div class="form-group form-group-lg">
			    <div class="col-sm-2">
			     	订单号：
			    </div>
			    <div class="col-sm-2">
			      ${data.agentOrderId}
			    </div>
			 </div>
			 <div class="form-group form-group-lg">
			    <div class="col-sm-2">
			    </div>
			    <div class="col-sm-2">
			    </div>
			 </div>
			 <div class="form-group form-group-lg">
			    <div class="col-sm-2">
			     	查询结果：
			    </div>
			    <div class="col-sm-2">
			      ${data.enumResult}
			    </div>
			 </div>
			 
		 </div>
		<div style="height:40px;">
			<div class="form-group form-group-lg">
			    <div class="col-sm-2">
			     	我方订单号：
			    </div>
			    <div class="col-sm-2" id="jnetOrderId">
			      
			    </div>
			 </div>
			 <div class="form-group form-group-lg">
			    <div class="col-sm-2">
			     	订单结果：
			    </div>
			    <div class="col-sm-2" id="payeeResult"></div>
			 </div>
		 </div>
		<div style="height:40px;">
			<div class="form-group form-group-lg">
			    <div class="col-sm-2">
			     	收款人姓名：
			    </div>
			    <div class="col-sm-2" id="payeeName2"></div>
			 </div>
			 <div class="form-group form-group-lg">
			    <div class="col-sm-2">
			     	收款人账号：
			    </div>
			    <div class="col-sm-2" id="payeeAccount2"></div>
			 </div>
			 <div class="form-group form-group-lg">
			    <div class="col-sm-2">
			     	付款结果信息：
			    </div>
			    <div class="col-sm-2" id="payMessage"></div>
			 </div>
			 <button type="button" class="btn btn-default" style="float: right;margin-right: 30px;" onclick="queryOrder()">查询订单</button>
		 </div>
		 <div style="height:40px;">
			<div class="form-group form-group-lg">
			    <div class="col-sm-2">
			     	账户余额：
			    </div>
			    <div class="col-sm-2" id="balance"></div>
			 </div>
			 <div class="form-group form-group-lg">
			    <div class="col-sm-2">
			     	可用余额：
			    </div>
			    <div class="col-sm-2" id="availableBalance"></div>
			 </div>
			 <div class="form-group form-group-lg">
			    <div class="col-sm-2">
			     	查询信息：
			    </div>
			    <div class="col-sm-2" id="retMsg"></div>
			 </div>
		 </div>
		 <button type="button" class="btn btn-default" style="float: right;margin-right: 30px;" onclick="balanceQuery()">查询余额</button>
	  </div>
	</div>
</body>
<script type="text/javascript">
	//支付方式的二级联动
	function payTypex(){
		if($("#payType").val() == 50){
			$("#selectBank").attr("style","display:block;");
		}else{
			$("#selectBank").attr("style","display:none;");
		}
	}
	//测试支付提交
	function testPay(){
		if($("#agentId").val() == ""){
			alert("商户id不能为空！");
			return false;
		}
		if($("#payAmt").val() == ""){
			alert("订单金额不能为空！");
			return false;
		}
		var s = validate($("#payAmt").val());
        $("#payAmt").val(s);   
		if($("#agentKey").val() == ""){
			alert("商户Key不能为空！");
			return false;
		}
		$("#testPayForm").submit();
	}
	//测试代付
	function testPayForAnother(){
		if($("#agentId2").val() == ""){
			alert("商户id不能为空！");
			return false;
		}
		if($("#payeeName").val() == ""){
			alert("收款人姓名不能为空！");
			return false;
		}
		if($("#payeeAccount").val() == ""){
			alert("收款人账号不能为空！");
			return false;
		}
		if($("#payeeOpeningBank").val() == ""){
			alert("收款支行名称不能为空！");
			return false;
		}
		if($("#province").val() == ""){
			alert("省份不能为空！");
			return false;
		}
		if($("#city").val() == ""){
			alert("城市不能为空！");
			return false;
		}
		if($("#amount").val() == ""){
			alert("订单金额不能为空！");
			return false;
		}
		var s = validate($("#amount").val());
		$("#amount").val(s);   
		if($("#agentKey2").val() == ""){
			alert("商户Key不能为空！");
			return false;
		}
		if($("#remark").val() == ""){
			alert("备注不能为空！");
			return false;
		}
		$("#queryForm").submit();
	}
	
	//文本框小数保留两位
	function validate(a){
		var f = Math.round(a*100)/100;    
        var s = f.toString();    
        var rs = s.indexOf('.');    
        if (rs < 0) {    
            rs = s.length;    
            s += '.';    
        }    
        while (s.length <= rs + 2) {    
            s += '0';    
        }    
        return s; 
	}
	
	//查询订单
	function queryOrder(){
		var version = $("#version").val();
		var agentId = $("#agentId2").val();
		var agentOrderId = $("#agentOrderId").val();
		var agentKey = $("#agentKey2").val();
		$.ajax({
			type: 'POST',
			url: '/testPay/queryOrder' ,
			contentType: "application/x-www-form-urlencoded; charset=utf-8",
		    data: {"version":version,"agentId":agentId,"agentOrderId":agentOrderId,"agentKey":agentKey}, 
		    success:function(datas) {
		    	$("#payeeResult").html(datas.payeeResult);
		    	$("#jnetOrderId").html(datas.jnetOrderId);
		    	$("#payeeName2").html(datas.payeeName);
		    	$("#payeeAccount2").html(datas.payeeAccount);
		    	$("#payMessage").html(datas.payMessage);
		    },
		    dataType: 'json'
		});
	}
	//查询余额
	function balanceQuery(){
		var agentId = $("#agentId2").val();
		var agentKey = $("#agentKey2").val();
		$.ajax({
			type: 'POST',
			url: '/testPay/balanceQuery' ,
			contentType: "application/x-www-form-urlencoded; charset=utf-8",
		    data: {"agentId":agentId,"agentKey":agentKey}, 
		    success:function(datas) {
		    	$("#balance").html(datas.balance);
		    	$("#availableBalance").html(datas.availableBalance);
		    	$("#retMsg").html(datas.retMsg);
		    },
		    dataType: 'json'
		});
	}
</script>
</html>
