package com.jvxin.testPay.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Random;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.jvxin.testPay.dto.HomeDto;
import com.jvxin.testPay.enums.EnumResultCode;
import com.jvxin.testPay.service.IHomeService;
import com.jvxin.testPay.util.Constant;

@Service
public class HomeServiceImpl implements IHomeService{
	
	public String testPay(String agentId,String payType,BigDecimal payAmt,String bankCode,String agentKey)throws Exception {
		//调用参数
		//版本号
		String version = "1.0";
		//提交单据时间
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		String orderTime = dateFormat.format( now ); 
		//商户订单号
		int Num = (int)((Math.random()*9+1)*10000000);
		String agentOrderId = "testPay_"+orderTime+Num ;
		//请求IP
		InetAddress address = InetAddress.getLocalHost();
		String payIp = address.getHostAddress();
		String notifyUrl = "localhost";
		if("50UN".equals(payType)){
			payType = "50";
			bankCode = "UNIONPAY";
		}
		if("50JD".equals(payType)){
			payType = "50";
			bankCode = "JDPAY";
		}
		//签名串
		String sign = version+"|"+agentId+"|"+agentOrderId+"|"+
					payType+"|"+payAmt+"|"+orderTime+"|"+payIp+"|"+notifyUrl+"|"+agentKey;
		//md5加密
		String result = HomeServiceImpl.encryption(sign);
		String url;
		if("50".equals(payType)&&StringUtils.isNotBlank(bankCode)){
			url = Constant.AGENT_URL+"/gateway/payment?version="+version+"&&agentId="+agentId+"&&agentOrderId="+agentOrderId+
					"&&payType="+payType+"&&bankCode="+bankCode+"&&payAmt="+payAmt+"&&orderTime="+orderTime+"&&payIp="+payIp+"&&notifyUrl="+notifyUrl+"&&sign="+result;    // 把字符串转换为URL请求地址
		}else {
			url = Constant.AGENT_URL+"/gateway/payment?version="+version+"&&agentId="+agentId+"&&agentOrderId="+agentOrderId+
					"&&payType="+payType+"&&payAmt="+payAmt+"&&orderTime="+orderTime+"&&payIp="+payIp+"&&notifyUrl="+notifyUrl+"&&sign="+result;    // 把字符串转换为URL请求地址
		}
		return "redirect:"+url;
	}

	@Override
	public HomeDto testPayForAnother(HomeDto dto) throws Exception {
		//版本号
		String version = "1.0";
		dto.setVersion(version);
		//提交单据时间
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		String orderTime = dateFormat.format( now ); 
		dto.setOrderTime(orderTime);
		//商户订单号
		int Num = (int)((Math.random()*9+1)*10000000);
		String agentOrderId = "testPayForAnother_"+orderTime+Num ;
		dto.setAgentOrderId(agentOrderId);
		//请求IP
		InetAddress address = InetAddress.getLocalHost();
		String payIp = address.getHostAddress();
		dto.setPayIp(payIp);
		//通知地址
		String notifyUrl = "localhost";
		dto.setNotifyUrl(notifyUrl);
		//签名串
		String sign = version+"|"+dto.getAgentId()+"|"+dto.getAgentOrderId()+"|"+dto.getBankCode()+"|"+dto.getPayeeType()+"|"+
					dto.getPayeeName()+"|"+dto.getPayeeAccount()+"|"+dto.getAmount()+"|"+dto.getNotifyUrl()+"|"+dto.getAgentKey();
		//md5加密
		String resultSign = HomeServiceImpl.encryption(sign);
		String param = "version="+version+"&agentId="+dto.getAgentId()+"&agentOrderId="+dto.getAgentOrderId()+
				"&bankCode="+dto.getBankCode()+"&payeeType="+dto.getPayeeType()+"&payeeName="+dto.getPayeeName()+"&payeeAccount="+dto.getPayeeAccount()+
				"&payeeOpeningBank="+dto.getPayeeOpeningBank()+"&province="+dto.getProvince()+"&city="+dto.getCity()+"&amount="+dto.getAmount()+
				"&orderTime="+dto.getOrderTime()+"&payIp="+dto.getPayIp()+"&notifyUrl="+dto.getNotifyUrl()+"&remark="+dto.getRemark()+"&sign="+resultSign;    // 把字符串转换为URL请求地址
		//发送 POST 请求
        String retCode = HomeServiceImpl.sendPost(Constant.AGENT_URL+"/withdraw/payment", param);
        String ss = new String(retCode.getBytes(), "utf-8");
        JSONObject jasonObject = JSONObject.fromObject(ss);
        Map map = (Map)jasonObject;
		String enumResult = EnumResultCode.get((String) map.get("retCode")).getDescription();
		dto.setRetCode((String) map.get("recode"));
		dto.setRetMsg((String) map.get("retMsg"));
		dto.setEnumResult(enumResult);
		return dto;
	}
	
	@Override
	public String queryOrder(String version, String agentId, String agentOrderId, String agentKey) throws Exception {
		//查询订单
		//签名串     
		String sign = version+"|"+agentId+"|"+agentOrderId+"|"+agentKey;
		String resultSign = HomeServiceImpl.encryption(sign);
		String param = "version="+version+"&agentId="+agentId+"&agentOrderId="+agentOrderId+
				"&sign="+resultSign;    // 把字符串转换为URL请求地址参数
		//发送 POST 请求
        String retCode = HomeServiceImpl.sendPost(Constant.AGENT_URL+"/withdraw/query", param);
        String dd = new String(retCode.getBytes(), "utf-8");
		return dd;
	}  
	
	public static String encryption(String sign) throws Exception{
		//MD5加密
		MessageDigest md5 = MessageDigest.getInstance("MD5");  
		md5.update((sign).getBytes("UTF-8"));  
		byte b[] = md5.digest();  
		int i;  
		StringBuffer buf = new StringBuffer("");  
		for(int offset=0; offset<b.length; offset++){  
		    i = b[offset];  
		    if(i<0){  
		        i+=256;  
		    }  
		    if(i<16){  
		        buf.append("0");  
		    }  
		    buf.append(Integer.toHexString(i));  
		}  
		String resultSign = buf.toString();  
		return resultSign;
	}
	
	public static String sendPost(String url, String param) {
	    PrintWriter out = null;
	    BufferedReader in = null;
	    String result = "";
	    try {
	        URL realUrl = new URL(url);
	        // 打开和URL之间的连接
	        URLConnection conn = realUrl.openConnection();
	        // 设置通用的请求属性
	        conn.setRequestProperty("accept", "*/*");
	        conn.setRequestProperty("connection", "Keep-Alive");
	        conn.setRequestProperty("user-agent",
	                "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
	        // 发送POST请求必须设置如下两行
	        conn.setDoOutput(true);
	        conn.setDoInput(true);
	        // 获取URLConnection对象对应的输出流
	        out = new PrintWriter(conn.getOutputStream());
	        // 发送请求参数
	        out.print(param);
	        // flush输出流的缓冲
	        out.flush();
	        // 定义BufferedReader输入流来读取URL的响应
	        in = new BufferedReader(
	                new InputStreamReader(conn.getInputStream()));
	        String line;
	        while ((line = in.readLine()) != null) {
	            result += line;
	        	}
		    } catch (Exception e) {
		        System.out.println("发送 POST 请求出现异常！"+e);
		        e.printStackTrace();
		    }
		    //使用finally块来关闭输出流、输入流
		    finally{
		        try{
		            if(out!=null){
		                out.close();
		            }
		            if(in!=null){
		                in.close();
		            }
		        }
		        catch(IOException ex){
		            ex.printStackTrace();
		        }
		    }
	    return result;
	}

	@Override
	public String balanceQuery(String agentId,String agentKey) throws Exception {
		//版本号
		String version = "1.0";
		//16位随机数
		Random ran = new Random();
		long random = ran.nextInt(99999999) + ran.nextInt(99999999);
		//签名数据
		String sign = version + "|" + agentId + "|" + random + "|" + agentKey;
		String resultSign = HomeServiceImpl.encryption(sign);
		
		String param = "version="+version+"&agentId="+agentId+"&random="+random+"&sign="+resultSign;
		//发送 POST 请求
        String retCode = HomeServiceImpl.sendPost(Constant.AGENT_URL+"/withdraw/balanceQuery", param);
        String ss = new String(retCode.getBytes(), "utf-8");
		return ss;
	}
	  
}
