package com.jvxin.testPay.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class HomeDto implements Serializable {
	
	/** 版本号 */
	private String version;
	/** 商户ID */
	private String agentId;
	/** 商户订单号 */
	private String agentOrderId;
	/** 银行编码 */
	private String bankCode;
	/** 对公对私 */
	private String payeeType;
	/** 收款人姓名 */
	private String payeeName;
	/** 收款人账号 */
	private String payeeAccount;
	/** 收款支行名称 */
	private String payeeOpeningBank;
	/** 省份 */
	private String province;
	/** 城市 */
	private String city;
	/** 订单金额 */
	private BigDecimal amount;
	/** 订单时间 */
	private String orderTime;
	/** 请求IP */
	private String payIp;
	/** 通知地址 */
	private String notifyUrl;
	/** 商户key */
	private String agentKey;
	/** 备注 */
	private String remark;
	/** 签名串 */
	private String sign;
	
	//-----调用接口后返回信息
	/** 返回码值 */
	private String retCode;
	/** 返回码信息提示 */
	private String retMsg;
	/** 返回码值的枚举 */
	private String enumResult;
	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public String getAgentOrderId() {
		return agentOrderId;
	}
	public void setAgentOrderId(String agentOrderId) {
		this.agentOrderId = agentOrderId;
	}
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public String getPayeeType() {
		return payeeType;
	}
	public void setPayeeType(String payeeType) {
		this.payeeType = payeeType;
	}
	public String getPayeeName() {
		return payeeName;
	}
	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}
	public String getPayeeAccount() {
		return payeeAccount;
	}
	public void setPayeeAccount(String payeeAccount) {
		this.payeeAccount = payeeAccount;
	}
	public String getPayeeOpeningBank() {
		return payeeOpeningBank;
	}
	public void setPayeeOpeningBank(String payeeOpeningBank) {
		this.payeeOpeningBank = payeeOpeningBank;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}
	public String getPayIp() {
		return payIp;
	}
	public void setPayIp(String payIp) {
		this.payIp = payIp;
	}
	public String getNotifyUrl() {
		return notifyUrl;
	}
	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}
	public String getAgentKey() {
		return agentKey;
	}
	public void setAgentKey(String agentKey) {
		this.agentKey = agentKey;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getRetCode() {
		return retCode;
	}
	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}
	public String getRetMsg() {
		return retMsg;
	}
	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}
	public String getEnumResult() {
		return enumResult;
	}
	public void setEnumResult(String enumResult) {
		this.enumResult = enumResult;
	}
	
}
