package com.jvxin.testPay.enums;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;


public enum EnumResultCode {

	SUCCESS("0000","执行成功"),
	PARAMETER_ERROR("E100","参数错误"),
	MERCHANTS_ERROR("E200","商户不存在"),
	SIGNATURE_ERROR("E201","签名验证错误"),
	NOT_SUFFICIENT_FUNDS("E202","余额不足"),
	BATCH_REPEAT("E203","批次重复"),
	SYSTEM_ERROR("U999","很抱歉，系统出现异常错误，确认操作是否成功");

	private final String code;
	private final String description;
	
	private static final Map<String,EnumResultCode> lookup = new HashMap<String,EnumResultCode>();

	static {
		for (EnumResultCode s : EnumSet.allOf(EnumResultCode.class)) {
			lookup.put(s.getCode(), s);
		}
	}

	public static EnumResultCode get(String code) {
		return lookup.get(code);
	}
	
	EnumResultCode(String code,String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}
	
}
