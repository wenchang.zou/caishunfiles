package com.jvxin.testPay.controller;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.jvxin.testPay.dto.HomeDto;
import com.jvxin.testPay.service.IHomeService;


/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	private IHomeService homeService;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 * agentId=asd&payType=40&payAmt=asd
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		
		return "home";
	}
	
	/**
	 * 调用支付接口
	 * @param 商户ID： agentId，支付方式：payType，订单金额：payAmt
	 * @return 返回网页支付页面
	 */
	@RequestMapping(value = "/testPay", method = RequestMethod.GET)
	public String testPay(String agentId,String payType,BigDecimal payAmt,String bankCode,String agentKey)throws NoSuchAlgorithmException, UnsupportedEncodingException {
		try {
			String url =  homeService.testPay(agentId, payType, payAmt,bankCode,agentKey);
			return url;
		} catch (Exception  e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 调用代付接口
	 * @param 商户ID：agentId，银行编号：bankCode，对公对私：payeeType，收款人姓名：payeeName，收款人账号：payeeAccount，收款人支行名称：payeeOpeningBank，
	 * 			省份：province，城市：city，订单金额：amount，备注 remark
	 * @return 返回申请信息。
	 */
	@RequestMapping(value = "/testPayForAnother", method = RequestMethod.GET)
	public ModelAndView testPayForAnother(HttpServletRequest request)throws NoSuchAlgorithmException, UnsupportedEncodingException {
		ModelAndView mv = new ModelAndView();
		HomeDto dto = new HomeDto();
		try {
			if(!StringUtils.isBlank(request.getParameter("agentId2"))) {
				dto.setAgentId(request.getParameter("agentId2"));
			}
			if(!StringUtils.isBlank(request.getParameter("bankCode2"))) {
				dto.setBankCode(request.getParameter("bankCode2"));
			}
			if(!StringUtils.isBlank(request.getParameter("payeeType"))) {
				dto.setPayeeType(request.getParameter("payeeType"));
			}
			if(!StringUtils.isBlank(request.getParameter("payeeName"))) {
				dto.setPayeeName(request.getParameter("payeeName"));
			}
			if(!StringUtils.isBlank(request.getParameter("payeeAccount"))) {
				dto.setPayeeAccount(request.getParameter("payeeAccount"));
			}
			if(!StringUtils.isBlank(request.getParameter("payeeOpeningBank"))) {
				dto.setPayeeOpeningBank(request.getParameter("payeeOpeningBank"));
			}
			if(!StringUtils.isBlank(request.getParameter("province"))) {
				dto.setProvince(request.getParameter("province"));
			}
			if(!StringUtils.isBlank(request.getParameter("city"))) {
				dto.setCity(request.getParameter("city"));
			}
			if(!request.getParameter("amount").equals(null)) {
				dto.setAmount(new BigDecimal(request.getParameter("amount")).setScale(2, RoundingMode.HALF_UP));
			}
			if(!StringUtils.isBlank(request.getParameter("agentKey2"))) {
				dto.setAgentKey(request.getParameter("agentKey2"));
			}
			if(!StringUtils.isBlank(request.getParameter("remark"))) {
				dto.setRemark(request.getParameter("remark"));
			}
			
			HomeDto result =  homeService.testPayForAnother(dto);
			
			mv.addObject("data",result);
			mv.setViewName("home");
			return mv;
		} catch (Exception  e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 查询订单。
	 * @param version,agentId,agentOrderId,agentKey
	 * @return
	 */
	@RequestMapping(value = "/queryOrder",produces="text/html;charset=UTF-8;")
	@ResponseBody
	public String queryOrder(String version,String agentId,String agentOrderId,String agentKey){
		try {
			return homeService.queryOrder(version,agentId,agentOrderId,agentKey);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 查询余额
	 * @param merchantId
	 * @return
	 */
	@RequestMapping(value = "/balanceQuery",produces="text/html;charset=UTF-8;")
	@ResponseBody
	public String balanceQuery(String agentId,String agentKey){
		try {
			return homeService.balanceQuery(agentId,agentKey);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
