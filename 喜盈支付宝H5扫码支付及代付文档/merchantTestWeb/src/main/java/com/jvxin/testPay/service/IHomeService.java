package com.jvxin.testPay.service;

import java.math.BigDecimal;

import com.jvxin.testPay.dto.HomeDto;

public interface IHomeService {
	
	public String testPay(String agentId,String payType,BigDecimal payAmt,String bankCode,String agentKey) throws Exception;

	public HomeDto testPayForAnother(HomeDto dto) throws Exception;

	public String balanceQuery(String agentId,String agentKey) throws Exception;

	public String queryOrder(String version, String agentId, String agentOrderId, String agentKey) throws Exception;
	
}
